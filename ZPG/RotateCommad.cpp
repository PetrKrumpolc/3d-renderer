#include "RotateCommad.h"
#include "Transformation.h"

#pragma region Usings
using Input::Commands::RotateComamnd;

using Core::User::Input::Mouse;

using Entities::Scene;
using Entities::Components::Transformation;
using Entities::Components::ComponentTypes;
#pragma endregion

#pragma region Construction & Destruction
RotateComamnd::RotateComamnd(Mouse& mouse, Scene& scene, float sensitivity)
	:_Mouse(mouse), 
	 _Scene(scene),
	 _Sensitivity(sensitivity)
{}

RotateComamnd::~RotateComamnd()
{
}
#pragma endregion

void RotateComamnd::Execute(float timeDelta)
{
	Transformation* transform = _Scene.GetActiveCamera()->GetComponent<Transformation>(ComponentTypes::Transformation);

	float horizontal = transform->GetRotation().GetYaw()   + (timeDelta * _Mouse.GetPositionDelta().GetX() * _Sensitivity);
	float vertical   = transform->GetRotation().GetPitch() + (timeDelta * _Mouse.GetPositionDelta().GetY() * _Sensitivity);

	if (vertical >= 90.0f)
		vertical = 89.0f;
	else if (vertical <= -90)
		vertical = -89.0f;

	if (horizontal >= 360.0f)
		horizontal = 0.0f;

	transform->GetRotation().SetYaw(horizontal);
	transform->GetRotation().SetPitch(vertical);
}
