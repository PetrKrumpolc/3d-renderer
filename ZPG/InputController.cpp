#include "InputController.h"
#include "GestureType.h"

#include <algorithm>

#pragma region Usings
using Input::InputController;
using Input::InputContext;

using Core::User::Input::IInputDevice;
using Core::User::Input::Keyboard;
using Core::User::Input::Mouse;
using Core::User::Input::Gestures::GestureType;
#pragma endregion

#pragma region Construction & Destruction
InputController::InputController(Mouse& mouse, Keyboard& keyboard)
{
	_Devices.push_back(&mouse);
	_Devices.push_back(&keyboard);
}

InputController::~InputController()
{
	if (_Context != nullptr)
		delete _Context;
}
#pragma endregion


void InputController::SetContext(InputContext* value)
{
	_Context = value;
}

void InputController::Update(float timeDelta)
{
	glfwPollEvents();

    if (_Context == nullptr)
		return;

    _Context->ForEach([this,timeDelta](Action* action) {
		std::for_each(_Devices.begin(), _Devices.end(), [&](IInputDevice* device) 
			{
				if (device->IsActive(*action->GetGesture()))
					action->Invoke(timeDelta);
			});
		});

	std::for_each(_Devices.begin(), _Devices.end(), [](IInputDevice* device) { device->Flush(); });
}
