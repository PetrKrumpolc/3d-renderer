#pragma once
#include <ostream>
#include <sstream>
#include <string>

namespace Core::IO
{
	class TextWriter
	{
	public:
		TextWriter(std::ostream * stream);
		~TextWriter();

		void Write(const std::string & text);
		void Write(const std::stringstream & stream);

	private:
		std::ostream * _Stream;
	};
}