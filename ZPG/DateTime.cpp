#include "DateTime.h"
#include "TimeFormats.h"

#include <sstream>
#include <iomanip>
#include <ctime>

#pragma region Usings
using Core::Time::DateTime;

using std::string;
using std::stringstream;
using std::time_t;

using std::chrono::system_clock;
using std::chrono::time_point;
#pragma endregion

#pragma region Statics
DateTime DateTime::Now()
{
	return DateTime(system_clock::now());
}
#pragma endregion

DateTime::DateTime(std::chrono::time_point<std::chrono::system_clock> time)
	:_Time(time)
{}


string DateTime::ToString() const
{
	return ToString(TimeFormats::FULL);
}

string DateTime::ToString(const string & format) const
{
	stringstream   output;
	struct tm      localTime;
	time_t         in_time_t = system_clock::to_time_t(_Time);

	localtime_s(&localTime, &in_time_t);
	output << std::put_time(&localTime, format.c_str());

	return output.str();
}
