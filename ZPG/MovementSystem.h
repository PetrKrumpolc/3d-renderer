#pragma once
#include "ISystem.h"
#include "BezierPath.h"
#include "BezierSpline.h"
#include "SceneObject.h"

#include "Scene.h"

#include "glm/vec3.hpp"

class MovementSystem : public Core::ISystem
{
public:
	MovementSystem(Entities::Scene * scene);
	~MovementSystem() = default;

	virtual void Update(float timeDelta) override;

private:
	Curves::BezierSpline _Bezier;
	Entities::Scene*	 _Scene;

	void TryMove(Entities::SceneObject* obj, float timeDelta);
	glm::vec3 MoveAlong(Entities::Components::BezierPath& path, float velocity);

};