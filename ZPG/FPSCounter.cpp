#include "FPSCounter.h"

#pragma region Usings
using Core::FPSCounter;
#pragma endregion


int FPSCounter::GetFPS() const
{
	return _FramesPerSecond;
}


FPSCounter::FPSCounter()
	:_AccumulatedTime(0.0),
	 _Frames(0),
	 _FramesPerSecond(0)
{}



void FPSCounter::Update(double elapsed)
{
	_AccumulatedTime += elapsed;
	_Frames++;

	if (_AccumulatedTime >= 1.0)
	{
		_FramesPerSecond = _Frames;
		Reset();
	}
}

void FPSCounter::Reset()
{
	_AccumulatedTime = 0.0;
	_Frames = 0;
}
