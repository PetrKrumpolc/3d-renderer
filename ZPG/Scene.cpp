#include "Scene.h"
#include "Transformation.h"

#pragma region Usings
using Entities::Scene;
using Entities::SceneObject;

using Entities::Components::Transformation;

using std::vector;
#pragma endregion

#pragma region Construction & Destruction
Scene::Scene()
	:_Background(new SceneObject()),
	 _Foreground(new SceneObject()),
	 _AmbientLight(nullptr),
	 _ActiveCamera(nullptr)
{
	_Background->SetName("Background");
	_Foreground->SetName("Foreground");

	_Background->Attach(new Transformation());
}

Scene::~Scene()
{
	delete _Background;
	delete _Foreground;

	for (SceneObject* spotLight : _PointLights)
		delete spotLight;
}
#pragma endregion


SceneObject& Scene::GetBackground()
{
	return *_Background;
}

SceneObject& Scene::GetForeground()
{
	return *_Foreground;
}

SceneObject* Scene::GetActiveCamera()
{
	return _ActiveCamera;
}

void Scene::SetActiveCamera(SceneObject* value)
{
	_ActiveCamera = value;
}


vector<SceneObject*> & Scene::GetPointLights()
{
	return _PointLights;
}

vector<SceneObject*>& Scene::GetSpotLights()
{
	return _SpotLights;
}

SceneObject* Scene::GetAmbientLight() const
{
	return _AmbientLight;
}

void Scene::SetAmbientLight(SceneObject* value)
{
	if (_AmbientLight != nullptr)
		delete _AmbientLight;

	_AmbientLight = value;
}
