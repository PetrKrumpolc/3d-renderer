#pragma once
#include "IComponent.h"
#include "SimpleProperty.h"

namespace Entities::Components
{
	class Movement : public IComponent
	{
	public:
		Core::SimpleProperty<float> Velocity;


		Movement(float velocity)
			: Velocity(velocity)
		{}
		virtual ~Movement() override {};

		virtual Entities::Components::ComponentTypes GetType() const noexcept override
		{
			return Entities::Components::ComponentTypes::Movement;
		}
	};
}