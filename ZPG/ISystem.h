#pragma once

namespace Core
{
	class ISystem
	{
	public:
		virtual ~ISystem() {};

		virtual void Update(float timeDelta) = 0;
	};
}