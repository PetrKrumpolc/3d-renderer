#pragma once
#include "Transformation.h"

namespace Input
{
	class MovementController
	{
	public:
		MovementController()  = default;
		~MovementController() = default;

		void MoveForward(float step, Entities::Components::Transformation & transformation);
		void MoveBackward(float step, Entities::Components::Transformation& transformation);
		void MoveLeft(float step, Entities::Components::Transformation& transformation);
		void MoveRight(float step, Entities::Components::Transformation& transformation);

	};
}