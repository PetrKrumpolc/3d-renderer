#pragma once
#include "ICommand.h"
#include "Application.h"

namespace Input::Commands
{
	class CloseCommand : public Input::Commands::ICommand
	{
	public:
		CloseCommand(Application & app)
			:_App(app)
		{}
		virtual ~CloseCommand() override {};

		virtual void Execute(float timeDelta) override
		{
			_App.GetMainWindow().Close();
		}

	private:
		Application& _App;
	};
}