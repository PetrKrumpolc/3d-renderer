#include "PrimitivesFactory.h"
#include "EntityFactory.h"
#include "Mesh.h"
#include "Transformation.h"
#include "Model.h"
#include "TextureLoader.h"

#include "Box.h"
#include "Plane.h"
#include "Sphere.h"
#include "SuziFlat.h"
#include "SuziSmooth.h"
#include "Worker.h"

static float TRIANGLE[] = {
	0.0f, 0.5f, 0.0f,
	0.5f, -0.5f, 0.0f,
	-0.5f, -0.5f, 0.0f
};

#pragma region Usings
using Rendering::Objects::PrimitivesFactory;
using Rendering::Objects::Mesh;
using Rendering::Objects::TextureLoader;

using Entities::SceneObject;
using Entities::EntityFactory;
using Entities::Components::ComponentTypes;
using Entities::Components::Transformation;
using Entities::Components::Model;
#pragma endregion



SceneObject* PrimitivesFactory::CreateTriangle() const
{
	SceneObject* triangle = new SceneObject();

	triangle->SetName("Primitive - Triagle");
	triangle->Attach(new Transformation());
	//triangle->Attach(new Mesh(TRIANGLE, 3));

	return triangle;
}

SceneObject* PrimitivesFactory::CreatePlane() const
{
	SceneObject* planeModel  = EntityFactory().CreateModel();
	SceneObject* planeObject = new SceneObject();

	Model* model = planeModel->GetComponent<Model>(ComponentTypes::Model);
	Mesh* plane  = new Mesh(Rendering::Objects::Primitives::PlaneVerticies, Rendering::Objects::Primitives::PlaneSize);

	model->GetMaterials().push_back(new Material());

	planeObject->SetName("Primitive - Plane");
	planeObject->Attach(new Transformation());
	planeObject->Attach(plane);

	plane->SetMaterial(model->GetMaterials().at(0));
	model->GetObjectContainer().Attach(planeObject);
	model->GetMaterials().at(0)->SetDiffuseMap(TextureLoader().LoadTexture("../Debug/texture.bmp", TextureTypes::Diffuse));

	return planeModel;
}

SceneObject* PrimitivesFactory::CreateBox() const
{
	SceneObject* box = new SceneObject();

	box->SetName("Primitive - Box");
	box->Attach(new Transformation());
	box->Attach(new Mesh(Rendering::Objects::Primitives::BoxVerticies, Rendering::Objects::Primitives::BoxSize));

	return box;
}

SceneObject* PrimitivesFactory::CreateSphere() const
{
	SceneObject* sphereModel = EntityFactory().CreateModel();
	SceneObject* sphereObject = new SceneObject();

	Model* model = sphereModel->GetComponent<Model>(ComponentTypes::Model);
	Mesh* sphere = new Mesh(Rendering::Objects::Primitives::SphereVerticies, Rendering::Objects::Primitives::SphereSize);

	model->GetMaterials().push_back(new Material());

	sphereObject->SetName("Primitive - Sphere");
	sphereObject->Attach(new Transformation());
	sphereObject->Attach(sphere);

	sphere->SetMaterial(model->GetMaterials().at(0));
	
	model->GetObjectContainer().Attach(sphereObject);

	return sphereModel;
}

SceneObject* PrimitivesFactory::CreateSuziFlat() const
{
	SceneObject* suzi = new SceneObject();

	suzi->SetName("Primitive - SuziFlat");
	suzi->Attach(new Transformation());
	suzi->Attach(new Mesh(Rendering::Objects::Primitives::SuziFlatVerticies, Rendering::Objects::Primitives::SuziFlatSize));

	return suzi;
}

SceneObject* PrimitivesFactory::CreateSuziSmooth() const
{
	SceneObject* suzi = new SceneObject();

	suzi->SetName("Primitive - SuziSmooth");
	suzi->Attach(new Transformation());
	suzi->Attach(new Mesh(Rendering::Objects::Primitives::SuziSmoothVerticies, Rendering::Objects::Primitives::SuziSmoothSize));

	return suzi;
}

SceneObject* PrimitivesFactory::CreateWorker() const
{
	SceneObject* worker = new SceneObject();

	worker->SetName("Primitive - Worker");
	worker->Attach(new Transformation());
	worker->Attach(new Mesh(Rendering::Objects::Primitives::WorkerVerticies, Rendering::Objects::Primitives::WorkerSize));

	return worker;
}
