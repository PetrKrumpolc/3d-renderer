#pragma once
#include "Event.h"

#include <glm/mat4x4.hpp>
#include <glm/gtc/type_ptr.hpp>

namespace Entities::Components
{
	class Rotation
	{
	public:
		Rotation();
		~Rotation();

		float GetYaw() const;
		void SetYaw(float value);

		float GetPitch() const;
		void SetPitch(float value);

		float GetRoll() const;
		void SetRoll(float value);

		const glm::mat4 & GetRotationMatrix() const;
		const glm::vec3 & GetDirection() const;

		Core::Events::Event<const glm::vec3&> DirectionChanged;

	private:
		float _Yaw;
		float _Pitch;
		float _Roll;

		glm::mat4 _RotationMatrix;
		glm::vec3 _Direction;

		Core::Events::EventInvoker<const glm::vec3 &> _OnDirectionChanged;

		void OnRotationChanged();

		glm::vec3 ComputeDirection(float yaw, float pitch)                  const;
		glm::mat4 ComputeRotationMatrix(float yaw, float pitch, float roll) const;
	};
}