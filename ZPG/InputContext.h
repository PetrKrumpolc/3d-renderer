#pragma once
#include "Action.h"

#include <forward_list>
#include <functional>

namespace Input
{
	class InputContext
	{
	public:
		InputContext();
		~InputContext();

		void ForEach(std::function<void(Action*)> function);

		void Add(Action* action);

	private:
		std::forward_list<Action*> _Actions;
	};
}