#pragma once
#include "ShaderPipelineTypes.h"
#include "Texture.h"
#include "SpotLight.h"
#include "PointLight.h"
#include "Transformation.h"

#include <GL\glew.h>
#include <glm/vec4.hpp>
#include <glm/mat4x4.hpp>
#include <string>

namespace Rendering::Shaders
{
	class Shader;

	class ShaderPipeline
	{
	public:
		ShaderPipeline(Rendering::Shaders::ShaderPipelineTypes type);
		~ShaderPipeline();

		bool IsReady()        const;
		std::string GetInfo() const;

		Rendering::Shaders::ShaderPipelineTypes GetType() const;

		void Use() const;
		void Unuse() const;

		void TrySetUniform(const std::string & name, const glm::vec4& value);
		void TrySetUniform(const std::string & name, const glm::mat4& value);
		void TrySetUniform(const std::string & name, const glm::vec3& value);
		void TrySetUniform(const std::string & name, const float value);
		void TrySetUniform(const std::string & name, const int value);
		void TrySetUniform(const std::string & name, const unsigned int value);
		void TrySetUniform(const std::string & name, const Rendering::Objects::Texture& texture);
		void TrySetUniform(const std::string & name, const Entities::Components::PointLight & light, const Entities::Components::Transformation & transform);
		void TrySetUniform(const std::string & name, const Entities::Components::SpotLight  & light, Entities::Components::Transformation & transform);

		void Attach(const Shader & shader);
		void Initialize();

	private:

		GLint GetInfoLength() const;
		GLint TryGetUniform(const std::string& name) const;

		GLuint										  _Pipeline;
		const Rendering::Shaders::ShaderPipelineTypes _Type;
	};
}