#pragma once

namespace Rendering::Shaders
{
	enum class ShaderPipelineTypes
	{
		Basic,
		LambertPhong,
		Sky
	};
}