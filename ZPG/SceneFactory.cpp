#include "SceneFactory.h"
#include "ModelImporter.h"
#include "Transformation.h"
#include "SceneObject.h"
#include "Movement.h"
#include "BezierPath.h"

#include "SpotLight.h"

#include <glm/gtc/type_ptr.hpp>

#pragma region Usings
using Entities::Scene;
using Entities::SceneFactory;
using Entities::SceneObject;

using Entities::Components::ComponentTypes;
using Entities::Components::Transformation;
using Entities::Components::SpotLight;
using Entities::Components::BezierPath;
using Entities::Components::Movement;

using Import::ModelImporter;

using std::string;

using glm::vec3;
#pragma endregion


Scene* SceneFactory::CreateSpheres() const
{
	Scene* scene = new Scene();

	scene->GetPointLights().push_back(_EntityFactory.CreatePointLight());
	scene->SetAmbientLight(_EntityFactory.CreateAmbientLight());

	SceneObject* sphere1 = _PrimitivesFactory.CreateSphere();
	SceneObject* sphere2 = _PrimitivesFactory.CreateSphere();
	SceneObject* sphere3 = _PrimitivesFactory.CreateSphere();
	SceneObject* sphere4 = _PrimitivesFactory.CreateSphere();

	sphere1->SetId(1);
	sphere2->SetId(2);
	sphere3->SetId(3);
	sphere4->SetId(4);

	sphere1->GetComponent<Transformation>(ComponentTypes::Transformation)->SetPosition(vec3(-1.0f, 0.0f, 0.0f));
	sphere1->GetComponent<Transformation>(ComponentTypes::Transformation)->SetScale(vec3(0.6f, 0.6f, 0.6f));
	sphere2->GetComponent<Transformation>(ComponentTypes::Transformation)->SetPosition(vec3(1.0f, 0.0f, 0.0f));
	sphere2->GetComponent<Transformation>(ComponentTypes::Transformation)->SetScale(vec3(0.6f, 0.6f, 0.6f));
	sphere3->GetComponent<Transformation>(ComponentTypes::Transformation)->SetPosition(vec3(0.0f, 0.0f, 1.0f));
	sphere3->GetComponent<Transformation>(ComponentTypes::Transformation)->SetScale(vec3(0.6f, 0.6f, 0.6f));
	sphere4->GetComponent<Transformation>(ComponentTypes::Transformation)->SetPosition(vec3(0.0f, 0.0f, -1.0f));
	sphere4->GetComponent<Transformation>(ComponentTypes::Transformation)->SetScale(vec3(0.6f, 0.6f, 0.6f));

	scene->GetForeground().Attach(sphere1);
	scene->GetForeground().Attach(sphere2);
	scene->GetForeground().Attach(sphere3);
	scene->GetForeground().Attach(sphere4);

	return scene;
}

Scene* SceneFactory::CreateMix() const
{
	Scene* scene = new Scene();

	scene->GetPointLights().push_back(_EntityFactory.CreatePointLight());
	scene->SetAmbientLight(_EntityFactory.CreateAmbientLight());

	SceneObject* worker		= _PrimitivesFactory.CreateWorker();
	SceneObject* suziFlat   = _PrimitivesFactory.CreateSuziFlat();
	SceneObject* suziSmooth = _PrimitivesFactory.CreateSuziSmooth();
	SceneObject* sphere		= _PrimitivesFactory.CreateSphere();

	worker->SetId(1);
	suziFlat->SetId(2);
	suziSmooth->SetId(3);
	sphere->SetId(4);

	worker->GetComponent<Transformation>(ComponentTypes::Transformation)->SetPosition(vec3(-2.0f, 0.0f, 0.0f));
	suziFlat->GetComponent<Transformation>(ComponentTypes::Transformation)->SetPosition(vec3(2.0f, 0.0f, 0.0f));
	suziSmooth->GetComponent<Transformation>(ComponentTypes::Transformation)->SetPosition(vec3(0.0f, 0.0f, 2.0f));
	sphere->GetComponent<Transformation>(ComponentTypes::Transformation)->SetPosition(vec3(0.0f, 0.0f, -2.0f));
	sphere->GetComponent<Transformation>(ComponentTypes::Transformation)->SetScale(vec3(0.2f, 0.2f, 0.2f));

	scene->GetForeground().Attach(worker);
	scene->GetForeground().Attach(suziFlat);
	scene->GetForeground().Attach(suziSmooth);
	scene->GetForeground().Attach(sphere);

	return scene;
}

Scene* SceneFactory::CreatePlane() const
{
	Scene* scene = new Scene();

	scene->GetPointLights().push_back(_EntityFactory.CreatePointLight());
	scene->GetPointLights().push_back(_EntityFactory.CreatePointLight(vec3(0.90, 0.39, 0.23), vec3(1.0, 0.67, 0.56)));
	scene->SetAmbientLight(_EntityFactory.CreateAmbientLight());

	scene->GetPointLights().at(0)->GetComponent<Transformation>(ComponentTypes::Transformation)->SetPosition(vec3(0.0f, 1.0f, 0.0f));
	scene->GetPointLights().at(1)->GetComponent<Transformation>(ComponentTypes::Transformation)->SetPosition(vec3(1.0f, 1.0f, 0.0f));

	SceneObject* plane = _PrimitivesFactory.CreatePlane();
	plane->SetId(1);

	scene->GetForeground().Attach(plane);

	return scene;
}

Scene* SceneFactory::FromModel(const string& path)
{
	Scene* scene = new Scene();

	scene->GetPointLights().push_back(_EntityFactory.CreatePointLight(vec3(0.0, 0.39, 0.0), vec3(0.0, 0.5, 0.0)));
	scene->GetPointLights().push_back(_EntityFactory.CreatePointLight(vec3(0.90, 0.39, 0.23), vec3(1.0, 0.67, 0.56)));
	scene->GetPointLights().push_back(_EntityFactory.CreatePointLight(vec3(0.96, 0.76, 0.25), vec3(1.0, 0.9, 0.65)));
	scene->GetPointLights().push_back(_EntityFactory.CreatePointLight());
	scene->SetAmbientLight(_EntityFactory.CreateAmbientLight());

	scene->GetPointLights().at(0)->GetComponent<Transformation>(ComponentTypes::Transformation)->SetPosition(vec3(0.0f, 12.0f, 0.0f));
	scene->GetPointLights().at(1)->GetComponent<Transformation>(ComponentTypes::Transformation)->SetPosition(vec3(5.0f, 0.0f, 0.0f));
	scene->GetPointLights().at(2)->GetComponent<Transformation>(ComponentTypes::Transformation)->SetPosition(vec3(0.0f, 1.0f, 12.0f));
	scene->GetPointLights().at(3)->GetComponent<Transformation>(ComponentTypes::Transformation)->SetPosition(vec3(-5.0f, 0.0f, 0.0f));


	SceneObject*    spotObject = _EntityFactory.CreateSpotLight();
	SpotLight*      spot       = spotObject->GetComponent<SpotLight>(ComponentTypes::SpotLight);
	Transformation* transform  = spotObject->GetComponent<Transformation>(ComponentTypes::Transformation);
	Movement*		lMovement  = new Movement(0.05f);
	BezierPath*		lPath	   = new BezierPath();

	lPath->Points().push_back(vec3(3.0f, 1.5f, -10.0f));
	lPath->Points().push_back(vec3(10.0f, 1.5f, -5.0f));
	lPath->Points().push_back(vec3(-10.0f, 1.5f, 5.0f));
	lPath->Points().push_back(vec3(3.0f, 1.5f, 10.0f));

	spotObject->Attach(lMovement);
	spotObject->Attach(lPath);

	spot->SetDiffuseColor(vec3(0.0, 0.39, 0.0));
	spot->SetSpecularColor(vec3(0.0, 0.5, 0.0));

	transform->SetPosition(vec3(3.0f, 1.0f, -10.0f));
	transform->GetRotation().SetYaw(-90);

	scene->GetSpotLights().push_back(spotObject);

	scene->GetForeground().Attach(ModelImporter(path).Import());
	scene->GetBackground().Attach(ModelImporter("../Debug/Models/sky/skydome.obj").Import());


	SceneObject*  cube = ModelImporter("../Debug/Models/TextureTestCobe.obj").Import();
	Movement* movement = new Movement(0.01f);
	BezierPath* bPath  = new BezierPath();

	bPath->Points().push_back(vec3(0.0f,   -1.0f, -12.0f));
	bPath->Points().push_back(vec3(10.0f,  -1.0f, -5.0f));
	bPath->Points().push_back(vec3(-10.0f,  -1.0f,  5.0f));
	bPath->Points().push_back(vec3(0.0f,   -1.0f,  12.0f));

	cube->Attach(movement);
	cube->Attach(bPath);

	scene->GetForeground().Attach(cube);

	return scene;
}

Scene* SceneFactory::CreateCubeTest() const
{
	Scene* scene = new Scene();

	scene->GetPointLights().push_back(_EntityFactory.CreatePointLight(vec3(0.90, 0.39, 0.23), vec3(1.0, 0.67, 0.56)));
	scene->SetAmbientLight(_EntityFactory.CreateAmbientLight());

	scene->GetPointLights().at(0)->GetComponent<Transformation>(ComponentTypes::Transformation)->SetPosition(vec3(1.0f, 3.0f, 1.0f));


	scene->GetForeground().Attach(ModelImporter("../Debug/Models/TextureTestCobe.obj").Import());
	scene->GetBackground().Attach(ModelImporter("../Debug/Models/sky/skydome.obj").Import());

	return scene;
}
