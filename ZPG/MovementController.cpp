#include "MovementController.h"

#include <glm/gtc/type_ptr.hpp>

#pragma region Usings
using Input::MovementController;

using Entities::Components::Transformation;

using glm::vec3;
#pragma endregion


void MovementController::MoveForward(float step, Transformation& transformation)
{
	transformation.SetPosition(transformation.GetPosition() + (transformation.GetRotation().GetDirection() * step));
}

void MovementController::MoveBackward(float step, Transformation& transformation)
{
	transformation.SetPosition(transformation.GetPosition() - (transformation.GetRotation().GetDirection() * step));
}

void MovementController::MoveLeft(float step, Transformation& transformation)
{
	vec3 left = glm::normalize(glm::cross(transformation.GetRotation().GetDirection(), vec3(0.0f, 1.0f, 0.0f)));
	transformation.SetPosition(transformation.GetPosition() - (left * step));
}

void MovementController::MoveRight(float step, Transformation& transformation)
{
	vec3 right = glm::normalize(glm::cross(transformation.GetRotation().GetDirection(), vec3(0.0f, 1.0f, 0.0f)));
	transformation.SetPosition(transformation.GetPosition() + (right * step));
}
