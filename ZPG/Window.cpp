#include "Window.h"

#pragma region Usings
using Core::User::Window;
using Core::User::Size;

using Core::User::Input::Keyboard;
using Core::User::Input::Mouse;

using std::string;
#pragma endregion

#pragma region Properties
Size Window::GetSize() const
{
	int width, height;
	glfwGetFramebufferSize(_Window, &width, &height);

	return Size(width, height);
}

Keyboard & Window::GetKeyboard()
{
	return *_Keyboard;
}

Mouse & Window::GetMouse()
{
	return *_Mouse;
}

void Window::SetTitle(const string& title)
{
	glfwSetWindowTitle(_Window, title.c_str());
}

void Window::SetVSync(bool enabled)
{
	glfwSwapInterval(enabled ? GL_TRUE : GL_FALSE);
}

bool Window::ShouldBeClosed() const
{
	return glfwWindowShouldClose(_Window);
}
#pragma endregion


#pragma region Construction & Destruction
Window::Window(const Size & size, const string & title)
	:Resized(Events::Event<const Size &>(&_ResizeInvoker))
{
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
	glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

	_Window = glfwCreateWindow(size.GetWidth(),
							   size.GetHeight(),
							   title.c_str(),
							   NULL, 
							   NULL);
	if (!_Window)
	{
		glfwTerminate();
		exit(EXIT_FAILURE);
	}
	else
		Init();
}

Window::~Window()
{
	delete _Keyboard;
	delete _Mouse;

	glfwDestroyWindow(_Window);
	glfwTerminate();
}
#pragma endregion

void Window::Init()
{
	glfwMakeContextCurrent(_Window);
	glfwSetWindowUserPointer(_Window,  this);

	_Keyboard = new Keyboard(*_Window);
	_Mouse    = new Mouse(*_Window);

	glfwSetWindowSizeCallback(_Window, [](GLFWwindow* window, int width, int height)
		{
			reinterpret_cast<Window*>(glfwGetWindowUserPointer(window))->_ResizeInvoker(Size(width, height));
		});
}



void Window::Show() const
{
	glfwShowWindow(_Window);
}

void Window::Refresh() const
{
	glfwSwapBuffers(_Window);
}

void Window::Close() const
{
	glfwSetWindowShouldClose(_Window, GL_TRUE);
}
