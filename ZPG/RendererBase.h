#pragma once
#include "SceneObject.h"
#include "Camera.h"

namespace Rendering
{
	class RendererBase
	{
	public:

		Entities::Components::Camera* GetCamera();
		void SetCamera(Entities::Components::Camera* value);

		RendererBase() = default;
		virtual ~RendererBase() {};

		virtual void Begin() {};
		virtual void Render(Entities::SceneObject& entity) = 0;
		virtual void End() {};

	protected:
		Entities::Components::Camera* _Camera;

		void DrawMesh(Entities::SceneObject& entity);
		bool IsDrawable(Entities::SceneObject& entity);
	};
}