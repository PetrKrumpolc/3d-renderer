#pragma once
#include <typeindex>
#include <forward_list>
#include <algorithm>

namespace Core::Events
{
	template<typename ... Args>
	class Event;

	template<typename ... Args>
	class EventInvoker
	{
		friend class Event<Args ...>;
	public:
		EventInvoker()  = default;
		~EventInvoker() = default;

		EventInvoker& operator()(Args ... args)
		{
			Invoke(args ...);
			return *this;
		}

		void Invoke(Args ... args) { _Event->Invoke(args ...); }

	private:
		Event<Args...>* _Event;
	};


	template<typename ... Args>
	class Event
	{
		friend class EventInvoker<Args ...>;
	private:
		class IHandler
		{
		public:
			virtual ~IHandler() {};
			virtual void Invoke(Args ... args) = 0;

			virtual bool Equals(const void* instance, const std::type_info& method) = 0;
		};

		template<typename Observer>
		class EventHandler : public IHandler
		{
		public:
			EventHandler(Observer * instance, void(Observer::*method)(Args...))
				: _Method(method),
				 _Instance(instance)
			{}
			virtual ~EventHandler() override {};

			virtual bool Equals(const void* instance, const std::type_info& method) override
			{
				return _Instance == instance && typeid(_Method) == method;
			}

			virtual void Invoke(Args ... args) override { (_Instance->*_Method)(args ...); }

		private:
			Observer* _Instance;
			void(Observer::* _Method)(Args...);
		};

		std::forward_list<IHandler*> _Handlers;

		void Invoke(Args ... args)
		{
			for (IHandler* handler : _Handlers)
				handler->Invoke(args...);
		}

		bool IsSubscribed(const void* instance, const std::type_info& method) const
		{
			return std::find_if(_Handlers.begin(), _Handlers.end(), [&](IHandler* h) { return (*h).Equals(instance, method); }) != _Handlers.end();
		}

	public:
		Event(EventInvoker<Args ...>* invoker)
		{
			invoker->_Event = this;
		}
		Event(const Event&) = delete;
		~Event()
		{
			std::for_each(_Handlers.begin(), _Handlers.end(), [](IHandler* handler) { delete handler; });
			_Handlers.clear();
		}

		template<typename Observer>
		void Subscribe(Observer * instance, void(Observer::*method)(Args...))
		{
			if(!IsSubscribed(instance, typeid(method)))
				_Handlers.push_front(new EventHandler<Observer>(instance, method));
		}

		template<typename Observer>
		void Unsubscribe(Observer* instance, void(Observer::* method)(Args...))
		{
			_Handlers.remove_if([&](IHandler* h) { return (*h).Equals(instance, typeid(method)); });
		}

		Event& operator=(const Event& right) = delete;
	};
}