#pragma once
#include "ComponentTypes.h"

namespace Entities
{
	class SceneObject;
}

namespace Entities::Components
{
	class IComponent
	{
		friend class Entities::SceneObject;
	public:
		virtual ~IComponent() {};

		virtual Entities::Components::ComponentTypes GetType() const noexcept = 0;

	private:
		void SetOwner(Entities::SceneObject* value);


	protected:
		Entities::SceneObject* _Owner;

		virtual void OnOwnerChanged(Entities::SceneObject * oldOwner, Entities::SceneObject * newOwner) {};
	};
}