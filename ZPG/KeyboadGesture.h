#pragma once
#include "IGesture.h"

namespace Core::User::Input::Gestures
{
	class KeyboardGesture : public Core::User::Input::Gestures::IGesture
	{
	public:
		KeyboardGesture(int button);
		KeyboardGesture(int button, int modifiers);
		virtual ~KeyboardGesture() override;

		int GetButton()    const;
		int GetModifiers() const;

	private:
		const int _Button;
		const int _Modiffiers;
	};
}