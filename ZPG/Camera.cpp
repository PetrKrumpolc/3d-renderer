#include "Camera.h"

#pragma region Usings
using Entities::Components::IComponent;
using Entities::Components::Camera;
using Entities::Components::ComponentTypes;

using Core::User::Size;

using glm::mat4;
using glm::vec3;
#pragma endregion

#pragma region Properties
const mat4 & Camera::GetProjectionMatrix() const
{
	return _ProjectionMatrix;
}

const mat4 & Camera::GetViewMatrix() const
{
	return _ViewMatrix;
}

void Camera::SetResolution(const Size & value)
{
	_Resolution = value;
	UpdateProjectionMatrix();
}

const Size& Camera::GetResolution() const
{
	return _Resolution;
}

void Camera::SetEye(const vec3 & value)
{
	_Eye = value;
	UpdateViewMatrix();
}

vec3 Camera::GetEye()
{
	return _Eye;
}

void Camera::SetDirection(const vec3 & value)
{
	_Direction = value;
	UpdateViewMatrix();
}


ComponentTypes Camera::GetType() const noexcept
{
	return ComponentTypes::Camera;
}
#pragma endregion

#pragma region Construction & Destruction
Camera::Camera(const Size & resolution, const vec3 & eye, const vec3 & direction, float fovDegree, float near, float far)
	: _Resolution(resolution),
	 _FieldOfView(glm::radians(fovDegree)),
	 _Near(near),
	 _Far(far),
	 _Eye(eye),
	 _Direction(direction)
{
	UpdateProjectionMatrix();
	UpdateViewMatrix();
}

Camera::~Camera()
{
}
#pragma endregion


void Camera::UpdateProjectionMatrix()
{
	_ProjectionMatrix = glm::perspective(_FieldOfView, static_cast<float>(_Resolution.GetWidth()) / _Resolution.GetHeight(), _Near, _Far);
}
void Camera::UpdateViewMatrix()
{
	_ViewMatrix = glm::lookAt(_Eye, _Eye + _Direction, vec3(0.0f, 1.0f, 0.0f));
}