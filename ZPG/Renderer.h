#pragma once
#include "ISystem.h"
#include "Viewport.h"
#include "ShaderPipelineRepository.h"
#include "Scene.h"
#include "SceneObject.h"
#include "SkyRenderer.h"
#include "WorldRenderer.h"

namespace Rendering
{
	class Renderer : public Core::ISystem
	{
	public:
		Renderer(const Rendering::Viewport & viewport, Entities::Scene & scene);
		~Renderer();

		void SetViewport(const Rendering::Viewport& value);

		virtual void Update(float timeDelta) override;

	private:
		void DrawBackground();
		void DrawForeground();

		Entities::Scene&				   _Scene;
		Shaders::ShaderPipelineRepository* _Shaders;

		Rendering::SkyRenderer*		_SkyRenderer;
		Rendering::WorldRenderer*	_WorldRenderer;
	};
}