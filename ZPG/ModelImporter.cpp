#include "ModelImporter.h"
#include "TextureLoader.h"
#include "Transformation.h"
#include "Mesh.h"
#include "Vertex.h"

#include <stdexcept>
#include <time.h> 

#include <glm/vec3.hpp>

#include <assimp/postprocess.h>


#pragma region Usings
using Import::ModelImporter;

using Entities::SceneObject;
using Entities::Components::Model;
using Entities::Components::Transformation;
using Entities::Components::ComponentTypes;
using Rendering::Objects::Mesh;
using Rendering::Objects::Vertex;
using Rendering::Objects::Material;
using Rendering::Objects::TextureLoader;
using Rendering::Objects::TextureTypes;
using Rendering::Objects::Texture;
using Rendering::Objects::MaterialConfig;

using std::string;
using std::vector;

using glm::vec3;
#pragma endregion


#pragma region Construction & Destruction
ModelImporter::ModelImporter(const string& path)
	: _Path(path)
{
	srand(time(NULL));

	_Importer = new Assimp::Importer();
	unsigned int importOptions = aiProcess_Triangulate
		| aiProcess_OptimizeMeshes
		| aiProcess_JoinIdenticalVertices
		| aiProcess_Triangulate
		| aiProcess_CalcTangentSpace;

	_ImportedScene = _Importer->ReadFile(path, importOptions);

	if (!_ImportedScene || _ImportedScene->mFlags == AI_SCENE_FLAGS_INCOMPLETE || !_ImportedScene->mRootNode) 
		throw std::runtime_error("ASSIMP - " + string(_Importer->GetErrorString()));
}

ModelImporter::~ModelImporter()
{
	delete _Importer;
}
#pragma endregion



SceneObject* ModelImporter::Import()
{
	SceneObject* modelObject = new SceneObject();
	modelObject->SetName(_Path);
	modelObject->Attach(new Transformation());
	modelObject->Attach(new Model());

	ImportMaterials(*modelObject->GetComponent<Model>(ComponentTypes::Model));
	ImportObjects(*modelObject->GetComponent<Model>(ComponentTypes::Model));

	return modelObject;
}



void ModelImporter::ImportMaterials(Model& model)
{
	for (unsigned int i = 0; i < _ImportedScene->mNumMaterials; i++)
		model.GetMaterials().push_back(ConvertMaterial(_ImportedScene->mMaterials[i]));
}

void ModelImporter::ImportObjects(Model& model)
{
	for (unsigned int i = 0; i < _ImportedScene->mNumMeshes; i++)
		model.GetObjectContainer().Attach(ConvertObject(_ImportedScene->mMeshes[i], model.GetMaterials()));
}


SceneObject* ModelImporter::ConvertObject(aiMesh* mesh, const vector<Material*> & materials)
{
	SceneObject* object = new SceneObject();
	object->SetId(rand() % 255);
	object->Attach(new Transformation());

	Vertex* pVertices = new Vertex[mesh->mNumVertices];
	std::memset(pVertices, 0, sizeof(Vertex) * mesh->mNumVertices);

	for (unsigned int i = 0; i < mesh->mNumVertices; i++)
	{
		if (mesh->HasPositions())
		{
			pVertices[i].Position[0] = mesh->mVertices[i].x;
			pVertices[i].Position[1] = mesh->mVertices[i].y;
			pVertices[i].Position[2] = mesh->mVertices[i].z;
		}
		if (mesh->HasNormals())
		{
			pVertices[i].Normal[0] = mesh->mNormals[i].x;
			pVertices[i].Normal[1] = mesh->mNormals[i].y;
			pVertices[i].Normal[2] = mesh->mNormals[i].z;
		}
		if (mesh->HasTextureCoords(0))
		{
			pVertices[i].UV[0] = mesh->mTextureCoords[0][i].x;
			pVertices[i].UV[1] = mesh->mTextureCoords[0][i].y;
		}
		if (mesh->HasTangentsAndBitangents())
		{
			pVertices[i].Tangent[0] = mesh->mTangents[i].x;
			pVertices[i].Tangent[1] = mesh->mTangents[i].y;
			pVertices[i].Tangent[2] = mesh->mTangents[i].z;
		}

	}

	unsigned int* pIndices = nullptr;

	if (mesh->HasFaces())
	{
		pIndices = new unsigned int[mesh->mNumFaces * 3];

		for (unsigned int i = 0; i < mesh->mNumFaces; i++)
		{
			pIndices[i * 3] = mesh->mFaces[i].mIndices[0];
			pIndices[i * 3 + 1] = mesh->mFaces[i].mIndices[1];
			pIndices[i * 3 + 2] = mesh->mFaces[i].mIndices[2];
		}
	}
	object->Attach(new Mesh(pVertices, mesh->mNumVertices, pIndices, mesh->mNumFaces * 3));
	object->GetComponent<Mesh>(ComponentTypes::Mesh)->SetMaterial(materials.at(mesh->mMaterialIndex));

	return object;
}

Material* ModelImporter::ConvertMaterial(aiMaterial* material)
{
	Material*        converted = new Material();
	MaterialConfig & config    = converted->GetConfiguration();

	aiString name;
	material->Get(AI_MATKEY_NAME, name);

	config.Diffuse   = ImportColor(material, AI_MATKEY_COLOR_DIFFUSE);
	config.Specular  = ImportColor(material, AI_MATKEY_COLOR_SPECULAR);
	config.Ambient   = ImportColor(material, AI_MATKEY_COLOR_AMBIENT);
	config.Shininnes = ImportFloat(material, AI_MATKEY_SHININESS);

	converted->SetDiffuseMap(ImportTexture(material,  aiTextureType_DIFFUSE,  TextureTypes::Diffuse));
	converted->SetSpecularMap(ImportTexture(material, aiTextureType_SPECULAR, TextureTypes::Specular));
	converted->SetNormalMap(ImportTexture(material,   aiTextureType_HEIGHT,   TextureTypes::Normal));

	converted->SetName(name.C_Str());

	return converted;
}

vec3 ModelImporter::ImportColor(aiMaterial* material, const char* key, unsigned int type, unsigned int index)
{
	aiColor4D color;

	if (AI_SUCCESS == aiGetMaterialColor(material, key, type, index, &color))
		return vec3(color.r, color.g, color.b);

	return vec3(0.0);
}

float ModelImporter::ImportFloat(aiMaterial* material, const char* key, unsigned int type, unsigned int index)
{
	float value;

	if (AI_SUCCESS == aiGetMaterialFloat(material, AI_MATKEY_SHININESS, &value))
		return value;

	return 0.0;
}

Texture* ModelImporter::ImportTexture(aiMaterial* material, aiTextureType type, TextureTypes customType)
{
	aiString path;

	if (material->GetTextureCount(type) > 0)
		if (material->GetTexture(type, 0, &path) == AI_SUCCESS)
			return TextureLoader().LoadTexture("../Debug/Textures/" + string(path.C_Str()), customType);

	return nullptr;
}
