#include "Texture.h"


#pragma region Usings
using Rendering::Objects::Texture;
using Rendering::Objects::TextureTypes;

using std::string;
#pragma endregion


#pragma region Properties
TextureTypes Texture::GetType() const
{
	return _Type;
}
#pragma endregion


#pragma region Construction & Destruction
Texture::Texture(const string & image, TextureTypes type)
	: _Type(type)
{
	glGenTextures(1, &_Texture);
}

Texture::~Texture()
{
	glDeleteTextures(1, &_Texture);
}
#pragma endregion


void Texture::Bind()
{
	glActiveTexture(static_cast<unsigned short>(_Type));
	glBindTexture(GL_TEXTURE_2D, _Texture);
}

void Texture::SetImage(size_t width, size_t height, const void* data, GLenum imageType)
{
	glBindTexture(imageType, _Texture);

	glTexParameteri(imageType, GL_TEXTURE_WRAP_S,	  GL_REPEAT);
	glTexParameteri(imageType, GL_TEXTURE_WRAP_T,	  GL_REPEAT);
	glTexParameteri(imageType, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(imageType, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

	glTexImage2D(imageType, 
				 0, 
				 GL_RGB, 
				 width, 
				 height, 
				 0, 
				 GL_RGB,
				 GL_UNSIGNED_BYTE, 
				 data);

	glGenerateMipmap(imageType);

	glBindTexture(imageType, 0);
}
