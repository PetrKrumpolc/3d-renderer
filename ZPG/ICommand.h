#pragma once

namespace Input::Commands
{
	class ICommand
	{
	public:
		virtual ~ICommand() {};

		virtual void Execute(float timeDelta) = 0;
	};
}