#pragma once

namespace Entities::Components
{
	enum class ComponentTypes
	{
		Transformation,
		Mesh,
		Camera,
		PointLight,
		AmbientLight,
		SpotLight,
		Model,
		BezierCubicPath,
		Movement
	};
}