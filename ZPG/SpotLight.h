#pragma once
#include "PointLight.h"

namespace Entities::Components
{
	class SpotLight : public Entities::Components::PointLight
	{
	public:
		float GetInnerCone() const;
		void SetInenrCone(float value);

		float GetOuterCone() const;
		void SetOuterCone(float value);

		SpotLight();
		virtual ~SpotLight() override;

		virtual Entities::Components::ComponentTypes GetType() const noexcept override;

	private:
		float _InnerCone;
		float _OuterCone;
	};
}