#include "ObjectFactory.h"

#pragma region Usings
using Rendering::Objects::ObjectFactory;
using Rendering::Objects::Mesh;
#pragma endregion


static float TRIANGLE[] = {
	0.0f, 0.5f, 0.0f,
	0.5f, -0.5f, 0.0f,
	-0.5f, -0.5f, 0.0f
};


Mesh* MeshFactory::CreateTriangle() const
{
	return nullptr;
}
