#pragma once
#include <GL/glew.h>
#include <GLFW/glfw3.h>

#include "IInputDevice.h"

namespace Core::User::Input
{
	class Keyboard : public IInputDevice
	{
	public:
		Keyboard(GLFWwindow & owner);
		virtual ~Keyboard() override;

		Core::User::Input::InputType GetType() const override;

		virtual void Flush() override;

		virtual bool IsActive(const Core::User::Input::Gestures::IGesture& gesture) const override;
		bool IsKeyPressed(int key) const;
		bool IsModifierPressed(int modifiers) const;

	private:
		GLFWwindow& _Owner;

		bool _IsKeyPressed[512];
		int  _Modifiers;

		void OnKeyChange(int key, int scancode, int action, int mods);
	};
}