#include "Keyboard.h"
#include "KeyboadGesture.h"
#include "Window.h"

#pragma region Usings
using Core::User::Window;
using Core::User::Input::Keyboard;
using Core::User::Input::InputType;
using Core::User::Input::Gestures::IGesture;
using Core::User::Input::Gestures::GestureType;
using Core::User::Input::Gestures::KeyboardGesture;
#pragma endregion

#pragma region Properties
bool Keyboard::IsKeyPressed(int key) const
{
	return _IsKeyPressed[key];
}

bool Keyboard::IsModifierPressed(int modifiers) const
{
	return _Modifiers == modifiers;
}

InputType Keyboard::GetType() const
{
	return InputType::Keyboard;
}
#pragma endregion


#pragma region Construction & Destruction
Keyboard::Keyboard(GLFWwindow & owner)
	:_Owner(owner),
	 _Modifiers(0)
{
	glfwSetKeyCallback(&owner, [](GLFWwindow* wnd, int key, int scancode, int action, int mods) 
	{ 
		reinterpret_cast<Window*>(glfwGetWindowUserPointer(wnd))->GetKeyboard().OnKeyChange(key, scancode, action, mods); 
	});

	for (int i = 0; i < 512; i++)
		_IsKeyPressed[i] = false;
}

Keyboard::~Keyboard()
{
	glfwSetKeyCallback(&_Owner, nullptr);
}
#pragma endregion


void Keyboard::OnKeyChange(int key, int scancode, int action, int mods)
{
	_IsKeyPressed[key] = action != GLFW_RELEASE;
	_Modifiers         = mods;
}

void Keyboard::Flush()
{
}

bool Keyboard::IsActive(const IGesture & gesture) const
{
	if (gesture.GetType() != GestureType::KeyStroke)
		return false;

	const KeyboardGesture & key = reinterpret_cast<const KeyboardGesture&>(gesture);

	return IsModifierPressed(key.GetModifiers()) && IsKeyPressed(key.GetButton());
}
