#include "ShaderPipeline.h"
#include "Shader.h"
#include "Application.h"

#include <stdexcept>

#include <glm/gtc/type_ptr.hpp>

#pragma region Usings
using Rendering::Shaders::Shader;
using Rendering::Shaders::ShaderPipeline;
using Rendering::Shaders::ShaderPipelineTypes;
using Rendering::Objects::Texture;

using Entities::Components::PointLight;
using Entities::Components::SpotLight;
using Entities::Components::Transformation;

using std::string;
using std::runtime_error;

using glm::vec3;
using glm::vec4;
using glm::mat4;
#pragma endregion

#pragma region Properties
bool ShaderPipeline::IsReady() const
{
	GLint status;
	glGetProgramiv(_Pipeline, GL_LINK_STATUS, &status);

	return status == GL_TRUE;
}

GLint ShaderPipeline::GetInfoLength() const
{
	GLint length;
	glGetProgramiv(_Pipeline, GL_INFO_LOG_LENGTH, &length);

	return length;
}

GLint ShaderPipeline::TryGetUniform(const string& name) const
{
	GLint uniform = glGetUniformLocation(_Pipeline, name.c_str());

	if (uniform >= 0)
		return uniform;
	else
		throw runtime_error("Variable <" + name + "> not exist.");
}

string ShaderPipeline::GetInfo() const
{
	GLint   length = GetInfoLength();
	GLchar* log    = new GLchar[length + 1];
	
	glGetProgramInfoLog(_Pipeline, length, NULL, log);

	string result(log);
	delete[] log;

	return result;
}


ShaderPipelineTypes ShaderPipeline::GetType() const
{
	return _Type;
}
#pragma endregion

#pragma region Construction & Destruction
ShaderPipeline::ShaderPipeline(ShaderPipelineTypes type)
	:_Type(type),
	 _Pipeline(glCreateProgram())
{}

ShaderPipeline::~ShaderPipeline()
{
	glDeleteProgram(_Pipeline);
}

void ShaderPipeline::Initialize()
{
	glLinkProgram(_Pipeline);

	if(GetInfoLength() != 0)
		Application::GetInstance().GetLogger().LogInfo(GetInfo());

	/*TrySetUniform("modelMatrix",	  glm::mat4(1.0f));
	TrySetUniform("projectionMatrix", glm::mat4(1.0f));
	TrySetUniform("viewMatrix",		  glm::mat4(1.0f));
	TrySetUniform("eyePosition",      glm::vec3(0.0f));
	TrySetUniform("pointLightCount",  0);

	TrySetUniform("material.Diffuse",  glm::vec3(0.0f));
	TrySetUniform("material.Specular", glm::vec3(1.0f));
	TrySetUniform("material.Ambient",  glm::vec3(0.05f));
	TrySetUniform("material.Shininnes", 32.0f);

	TrySetUniform("diffuseMap", 0);

	for (int i = 0; i < 16; i++)
	{
		TrySetUniform("pointLights[" + std::to_string(i) + "].Position", glm::vec3(0.0f));

		TrySetUniform("pointLights[" + std::to_string(i) + "].Diffuse", glm::vec3(1.0f));
		TrySetUniform("pointLights[" + std::to_string(i) + "].Specular", glm::vec3(0.0f));
	}*/
}
#pragma endregion

#pragma region Program
void ShaderPipeline::Use() const
{
	glUseProgram(_Pipeline);
}

void ShaderPipeline::Unuse() const
{
	glUseProgram(NULL);
}
#pragma endregion

#pragma region Uniforms
void ShaderPipeline::TrySetUniform(const string & name, const vec4& value)
{
	glUniform4f(TryGetUniform(name), value.x, value.y, value.z, value.w);
}

void ShaderPipeline::TrySetUniform(const string & name, const mat4& value)
{
	glUniformMatrix4fv(TryGetUniform(name), 1, GL_FALSE, glm::value_ptr(value));
}
void ShaderPipeline::TrySetUniform(const string& name, const vec3 & value)
{
	glUniform3f(TryGetUniform(name), value.x, value.y, value.z);
}
void ShaderPipeline::TrySetUniform(const string& name, const float value)
{
	glUniform1f(TryGetUniform(name), value);
}
void ShaderPipeline::TrySetUniform(const string& name, const int value)
{
	glUniform1i(TryGetUniform(name), value);
}
void ShaderPipeline::TrySetUniform(const string& name, const unsigned int value)
{
	glUniform1ui(TryGetUniform(name), value);
}
void ShaderPipeline::TrySetUniform(const string& name, const Texture& texture)
{
	glUniform1i(TryGetUniform(name), static_cast<unsigned short>(texture.GetType()) - GL_TEXTURE0);
}
void ShaderPipeline::TrySetUniform(const string& name, const PointLight& light, const Transformation& transform)
{
	TrySetUniform(name + ".Position", transform.GetPosition());

	TrySetUniform(name + ".Diffuse",	light.GetDiffuseColor());
	TrySetUniform(name + ".Specular",	light.GetSpecularColor());

	TrySetUniform(name + ".Constant",	light.GetConstant());
	TrySetUniform(name + ".Linear",		light.GetLinear());
	TrySetUniform(name + ".Quadratic",	light.GetQuadratic());

	TrySetUniform(name + ".Power",		light.GetPower());
}

void ShaderPipeline::TrySetUniform(const string& name, const SpotLight& light, Transformation& transform)
{
	TrySetUniform(name + ".Base", static_cast<PointLight>(light), transform);

	TrySetUniform(name + ".Direction", transform.GetRotation().GetDirection());

	TrySetUniform(name + ".InnerCone", light.GetInnerCone());
	TrySetUniform(name + ".OuterCone", light.GetOuterCone());
}
#pragma endregion

void ShaderPipeline::Attach(const Shader & shader)
{
	glAttachShader(_Pipeline, shader._Shader);
}