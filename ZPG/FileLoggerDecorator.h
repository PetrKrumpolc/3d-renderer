#pragma once
#include "ILogger.h"

#include <string>
#include <sstream>
#include <queue>

namespace Core::Debug
{
	class FileLoggerDecorator : public ILogger
	{
	public:
		FileLoggerDecorator(std::string path, ILogger * logger);
		virtual ~FileLoggerDecorator() override;

		virtual void LogError(const std::string & message)   override;
		virtual void LogInfo(const std::string & message)    override;
		virtual void LogWarning(const std::string & message) override;
		virtual void LogDebug(const std::string & message)   override;

		virtual void Flush() override;

	private:

		void ResetCache();
		void AppendToCache(const std::string& message);

		void AfterLogging(const std::string & message);

		void TrySaveToFile();

		int				  _CacheSize;
		const int		  _CacheLimit;
		std::stringstream _Cache;

		const std::string _Path;

		ILogger * _Logger;
	};
}