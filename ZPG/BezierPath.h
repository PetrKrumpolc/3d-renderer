#pragma once
#include "IComponent.h"

#include <vector>
#include "glm/vec3.hpp"

#include "SimpleProperty.h"

namespace Entities::Components
{
	class BezierPath : public IComponent
	{
	public:
		Core::SimpleProperty<std::vector<glm::vec3>> Points;
		Core::SimpleProperty<float>					 Coeficient;

		BezierPath()
			: Points(),
			  Coeficient(0.0f)
		{}
		virtual ~BezierPath() override {};

		virtual Entities::Components::ComponentTypes GetType() const noexcept override
		{
			return Entities::Components::ComponentTypes::BezierCubicPath;
		}
	};
}