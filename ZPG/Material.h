#pragma once
#include <glm/vec3.hpp>

#include <string>

#include "Texture.h"
#include "MaterialSettings.h"

namespace Rendering::Objects
{
	struct MaterialConfig
	{
		glm::vec3 Diffuse;
		glm::vec3 Specular;
		glm::vec3 Ambient;

		float Shininnes;

		MaterialConfig()
			: Diffuse(glm::vec3(0.385f, 0.647f, 0.812f)),
			  Specular(glm::vec3(1.0f)),
			  Ambient(glm::vec3(0.05f)),
			  Shininnes(32)
		{}
	};

	class Material
	{
	public:
		const std::string& GetName() const;
		void SetName(const std::string & value);

		Rendering::Objects::MaterialSettings& GetSettings();

		Material();
		~Material();

		Rendering::Objects::Texture* GetDiffuseMap();
		void SetDiffuseMap(Rendering::Objects::Texture* value);

		Rendering::Objects::Texture* GetNormalMap();
		void SetNormalMap(Rendering::Objects::Texture* value);

		Rendering::Objects::Texture* GetSpecularMap();
		void SetSpecularMap(Rendering::Objects::Texture* value);

		Rendering::Objects::Texture* GetOcclusionMap();
		void SetOcclusionMap(Rendering::Objects::Texture* value);

		Rendering::Objects::MaterialConfig& GetConfiguration();

	private:
		std::string _Name;

		Rendering::Objects::MaterialSettings _Settings;
		Rendering::Objects::MaterialConfig	 _Config;

		Rendering::Objects::Texture* _DiffuseMap;
		Rendering::Objects::Texture* _NormalMap;
		Rendering::Objects::Texture* _SpecularMap;
		Rendering::Objects::Texture* _OcclusionMap;

		void SetOption(MaterialSettings option, bool value);
	};
}