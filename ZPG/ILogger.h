#pragma once
#include <string>
#include <chrono>

namespace Core::Debug
{
	class ILogger
	{
	public:
		virtual ~ILogger() = default;

		virtual void LogError(const std::string & message)   = 0;
		virtual void LogInfo(const std::string & message)    = 0;
		virtual void LogWarning(const std::string & message) = 0;
		virtual void LogDebug(const std::string & message)   = 0;

		virtual void Flush() = 0;

	protected:
		std::string FormatLogMessage(const std::string & type, const std::string & message, const std::string& timeFormat);
	};
}