#pragma once
#include "IComponent.h"
#include <glm/gtc/type_ptr.hpp>

namespace Entities::Components
{
	class AmbientLight : public Entities::Components::IComponent
	{
	public:
		AmbientLight();
		~AmbientLight();

		virtual Entities::Components::ComponentTypes GetType() const noexcept override;

		const glm::vec3& GetAmbientColor() const;
		void SetAmbientColor(const glm::vec3& value);

	private:
		glm::vec3 _AmbientColor;
	};
}