#pragma once

namespace Core::User
{
	class Size
	{
	public:
		Size(int width, int height);
		~Size() = default;

		int GetWidth()  const;
		int GetHeight() const;

		Size& operator =(const Size& size);

	private:
		int _Width;
		int _Height;
	};
}