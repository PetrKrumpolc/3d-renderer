#include "Mesh.h"

#pragma region Usings
using Rendering::Objects::Mesh;
using Rendering::Objects::Vertex;
using Rendering::Objects::Material;

using Entities::Components::ComponentTypes;
#pragma endregion

#pragma region Properties
size_t Mesh::GetVertexCount() const
{
	return _Verticies->GetSize() / sizeof(Vertex);
}

size_t Mesh::GetIndexCount() const
{
	return _Indicies == nullptr ? 0 : _Indicies->GetSize() / sizeof(int);
}

Material* Mesh::GetMaterial()
{
	return _Material;
}

void Mesh::SetMaterial(Material* value)
{
	_Material = value;
}

ComponentTypes Mesh::GetType() const noexcept
{
	return ComponentTypes::Mesh;
}
#pragma endregion

#pragma region Construction & Destruction
Mesh::Mesh(Vertex* verticies, size_t vertexCount)
{
	_Verticies		= new Buffer(verticies, vertexCount * sizeof(Vertex), GL_ARRAY_BUFFER, GL_STATIC_DRAW);
	_VertexLayout	= new VertexLayout();

	_VertexLayout->AddChunk(0, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (GLvoid*)0);
	_VertexLayout->AddChunk(1, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (GLvoid*)(3 * sizeof(float)));
	_VertexLayout->AddChunk(2, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex), (GLvoid*)(6 * sizeof(float)));
	_VertexLayout->AddChunk(3, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (GLvoid*)(8 * sizeof(float)));
}

Mesh::Mesh(Vertex* verticies, size_t vertexCount, const unsigned int* indicies, size_t indexCount)
{
	_Verticies	  = new Buffer(verticies, vertexCount * sizeof(Vertex), GL_ARRAY_BUFFER, GL_STATIC_DRAW);
	_Indicies	  = new Buffer(indicies, indexCount * sizeof(unsigned int), GL_ELEMENT_ARRAY_BUFFER, GL_STATIC_DRAW);
	_VertexLayout = new VertexLayout();

	_VertexLayout->AddChunk(0, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (GLvoid*)0);
	_VertexLayout->AddChunk(1, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (GLvoid*)(3 * sizeof(float)));
	_VertexLayout->AddChunk(2, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex), (GLvoid*)(6 * sizeof(float)));
	_VertexLayout->AddChunk(3, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (GLvoid*)(8 * sizeof(float)));
}

Mesh::~Mesh()
{
	if (_Indicies != nullptr)
		delete _Indicies;

	delete _Verticies;
	delete _VertexLayout;
}
#pragma endregion

void Mesh::Bind() const
{
	_VertexLayout->Bind();
	if(_Indicies != nullptr)
		_Indicies->Bind();
}
