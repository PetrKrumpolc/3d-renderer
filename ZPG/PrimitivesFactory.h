#pragma once
#include "SceneObject.h"

namespace Rendering::Objects
{
	class PrimitivesFactory
	{
	public:
		PrimitivesFactory()  = default;
		~PrimitivesFactory() = default;

		Entities::SceneObject* CreateTriangle() const;
		Entities::SceneObject* CreatePlane()    const;

		Entities::SceneObject* CreateBox()        const;
		Entities::SceneObject* CreateSphere()     const;
		Entities::SceneObject* CreateSuziFlat()   const;
		Entities::SceneObject* CreateSuziSmooth() const;
		Entities::SceneObject* CreateWorker()     const;
	};
}