#include "ConsoleLogger.h"
#include "LogTypes.h"
#include "TimeFormats.h"

#include <iostream>

#pragma region Usings
using Core::Debug::ConsoleLogger;

using std::string;

using std::cout;
using std::endl;
#pragma endregion

#pragma region Construction & Destruction
ConsoleLogger::ConsoleLogger()
	: ILogger()
{
#ifdef OS_WIN
	_Console = GetStdHandle(STD_OUTPUT_HANDLE);
#endif // OS_WIN
}

ConsoleLogger::~ConsoleLogger()
{
#ifdef OS_WIN
	CloseHandle(_Console);
#endif // OS_WIN
}
#pragma endregion

#pragma region ILogger
void ConsoleLogger::LogError(const string & message)
{
#ifdef OS_WIN
	SetConsoleTextAttribute(_Console, FOREGROUND_RED);
#endif // OS_WIN
	PrintMessage(Core::Debug::LogTypes::ERROR_TYPE, message);
}

void ConsoleLogger::LogInfo(const string & message)
{
#ifdef OS_WIN
	SetConsoleTextAttribute(_Console, FOREGROUND_GREEN | FOREGROUND_INTENSITY );
#endif // OS_WIN
	PrintMessage(Core::Debug::LogTypes::INFO_TYPE, message);
}

void ConsoleLogger::LogWarning(const string & message)
{
#ifdef OS_WIN
	SetConsoleTextAttribute(_Console, FOREGROUND_GREEN | FOREGROUND_RED);
#endif // OS_WIN
	PrintMessage(Core::Debug::LogTypes::WARNING_TYPE, message);
}

void ConsoleLogger::LogDebug(const string & message)
{
#ifdef LOG_DEBUG
#ifdef OS_WIN
	SetConsoleTextAttribute(_Console, FOREGROUND_BLUE | FOREGROUND_INTENSITY);
#endif // OS_WIN
	PrintMessage(Core::Debug::LogTypes::DEBUG_TYPE, message);
#endif // LOG_DEBUG
}

void ConsoleLogger::Flush()
{
	std::flush(cout);
}
#pragma endregion


void ConsoleLogger::PrintMessage(const string & type, const string & message)
{
	cout << FormatLogMessage(type, message, Time::TimeFormats::HHMMSS) << endl;
#ifdef OS_WIN
	SetConsoleTextAttribute(_Console, FOREGROUND_RED | FOREGROUND_GREEN | FOREGROUND_BLUE | FOREGROUND_INTENSITY);
#endif // OS_WIN
}
