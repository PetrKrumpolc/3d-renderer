#pragma once
#include "ActionType.h"
#include "IGesture.h"
#include "ICommand.h"

namespace Input
{
	class Action
	{
	public:
		Action(Input::ActionType type);
		Action(Input::ActionType type, Core::User::Input::Gestures::IGesture * gesture);
		Action(Input::ActionType type, Core::User::Input::Gestures::IGesture * gesture, Input::Commands::ICommand * command);
		~Action();

		void Invoke(float timeDelta) const;

		void SetGesture(Core::User::Input::Gestures::IGesture * value);
		Core::User::Input::Gestures::IGesture* GetGesture();

		void SetCommand(Input::Commands::ICommand * command);

	private:
		const Input::ActionType				    _Type;
		Core::User::Input::Gestures::IGesture * _Gesture;
		Input::Commands::ICommand *				_Command;
	};
}