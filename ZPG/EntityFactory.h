#pragma once
#include "SceneObject.h"

#include <glm/vec3.hpp>

namespace Entities
{
	class EntityFactory
	{
	public:
		EntityFactory()  = default;
		~EntityFactory() = default;

		SceneObject* CreateCamera() const;
		SceneObject* CreatePointLight() const;
		SceneObject* CreatePointLight(glm::vec3 diffuse, glm::vec3 specular) const;
		SceneObject* CreateSpotLight() const;
		SceneObject* CreateAmbientLight() const;
		SceneObject* CreateModel() const;
	};
}