#pragma once

namespace Rendering::Shaders
{
	enum class ShaderTypes
	{
		Vertex   = GL_VERTEX_SHADER,
		Fragment = GL_FRAGMENT_SHADER
	};
}