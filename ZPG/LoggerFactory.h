#pragma once
#include "ILoggerFactory.h"

#include <string>

namespace Core::Debug
{
	class LoggerFactory : public Core::Debug::ILoggerFactory
	{
	public:
		LoggerFactory()  = default;
		~LoggerFactory() = default;

		virtual ILogger* CreateLogger() const override;

	private:
		std::string GetFileName() const;
	};
}