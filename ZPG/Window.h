#pragma once
#include <GL/glew.h>
#include <GLFW\glfw3.h>

#include "Size.h"
#include "Keyboard.h"
#include "Mouse.h"

#include <string>
#include <functional>

#include "Event.h"
#include "ReadOnlyProperty.h"

namespace Core::User
{
	class Window
	{
	public:
		Window(const Core::User::Size & size, const std::string & title);
		~Window();

		void Show()    const;
		void Refresh() const;
		void Close()   const;

		void SetTitle(const std::string& title);
		void SetVSync(bool enabled);

		Core::User::Size			  GetSize() const;
		Core::User::Input::Keyboard & GetKeyboard();
		Core::User::Input::Mouse &	  GetMouse();

		bool ShouldBeClosed() const;

		Core::Events::Event<const Core::User::Size &> Resized;

	private:
		void Init();

		GLFWwindow*				     _Window;
		Core::User::Input::Keyboard* _Keyboard;
		Core::User::Input::Mouse*    _Mouse;

		Core::Events::EventInvoker<const Core::User::Size &> _ResizeInvoker;
	};
}