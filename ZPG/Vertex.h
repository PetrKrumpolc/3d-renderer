#pragma once
namespace Rendering::Objects
{
	struct Vertex
	{
		float Position[3];
		float Normal[3];
		float UV[2];
		float Tangent[3];
	};
}