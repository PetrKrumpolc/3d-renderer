#pragma once
#include "GestureType.h"

namespace Core::User::Input::Gestures
{
	class IGesture
	{
	public:
		IGesture(Core::User::Input::Gestures::GestureType type)
			:_Type(type)
		{}
		virtual ~IGesture() {}

		Core::User::Input::Gestures::GestureType GetType() const
		{
			return _Type;
		}

	private:
		const Core::User::Input::Gestures::GestureType _Type;
	};
}