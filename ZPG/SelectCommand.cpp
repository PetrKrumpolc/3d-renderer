#include "SelectCommand.h"
#include "Application.h"

#include "Camera.h"

#include <string>

#pragma region Usings
using Input::Commands::SelectCommand;

using Core::User::Window;

using Entities::Components::Camera;
using Entities::Components::ComponentTypes;
#pragma endregion

#pragma region Construction & Destruction
SelectCommand::SelectCommand(Window& window, Entities::Scene& scene)
	: _Window(window),
	  _Scene(scene)
{
}

SelectCommand::~SelectCommand()
{
}
#pragma endregion



void SelectCommand::Execute(float timeDelta)
{
	GLubyte  color[4];
	GLfloat depth;
	GLuint  index;
	Camera* camera = _Scene.GetActiveCamera()->GetComponent<Camera>(ComponentTypes::Camera);

	GLint x = (GLint)_Window.GetMouse().GetPosition().GetX();
	GLint y = (GLint)_Window.GetMouse().GetPosition().GetY();

	int newy = camera->GetResolution().GetHeight() - y;

	glReadPixels(x, newy, 1, 1, GL_RGBA,            GL_UNSIGNED_BYTE, color);
	glReadPixels(x, newy, 1, 1, GL_DEPTH_COMPONENT, GL_FLOAT,         &depth);
	glReadPixels(x, newy, 1, 1, GL_STENCIL_INDEX,   GL_UNSIGNED_INT,  &index);

	Application::GetInstance().GetLogger().LogInfo("Clicked on pixel at " + _Window.GetMouse().GetPosition().ToString());
	Application::GetInstance().GetLogger().LogInfo("\tColor " + std::to_string(color[0]) + " " + std::to_string(color[1]) + " " + std::to_string(color[2]) + " " + std::to_string(color[3]));
	Application::GetInstance().GetLogger().LogInfo("\tDepth " + std::to_string(depth));
	Application::GetInstance().GetLogger().LogInfo("\tStencil Index " + std::to_string(index));

	glm::vec3 screenX = glm::vec3(x, newy, depth);
	const glm::mat4& view       = camera->GetViewMatrix();
	const glm::mat4& projection = camera->GetProjectionMatrix();

	const glm::vec4& viewPort = glm::vec4(0, 0,
		camera->GetResolution().GetWidth(),
		camera->GetResolution().GetHeight());
	glm::vec3 pos = glm::unProject(screenX, view, projection, viewPort);

	Application::GetInstance().GetLogger().LogInfo("\tunProject " + std::to_string(pos.x) + " " + std::to_string(pos.y) + " " + std::to_string(pos.z));
}
