#pragma once
#include "IComponent.h"
#include "Vertex.h"
#include "VertexLayout.h"
#include "Buffer.h"
#include "Material.h"

#include <GL\glew.h>

namespace Rendering::Objects
{
	class Mesh : public Entities::Components::IComponent
	{
	public:
		Entities::Components::ComponentTypes GetType() const noexcept override;

		size_t GetVertexCount() const;
		size_t GetIndexCount()  const;

		Rendering::Objects::Material * GetMaterial();
		void SetMaterial(Rendering::Objects::Material* value);

		Mesh(Rendering::Objects::Vertex* verticies, size_t vertexCount);
		Mesh(Rendering::Objects::Vertex* verticies, size_t vertexCount, const unsigned int * indicies, size_t indexCount);
		virtual ~Mesh() override;

		void Bind() const;

	private:
		Rendering::Objects::Buffer*			_Indicies;
		Rendering::Objects::Buffer*			_Verticies;
		Rendering::Objects::VertexLayout*	_VertexLayout;

		Rendering::Objects::Material* _Material;
	};
}