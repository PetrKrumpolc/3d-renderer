#include "Renderer.h"
#include "ShaderPipelineTypes.h"
#include "ComponentTypes.h"
#include "Transformation.h"
#include "Camera.h"

#pragma region Usings
using Rendering::Renderer;
using Rendering::Viewport;
using Rendering::SkyRenderer;
using Rendering::WorldRenderer;
using Rendering::Shaders::ShaderPipelineRepository;

using Entities::Scene;
using Entities::SceneObject;
using Entities::Components::Camera;
using Entities::Components::Transformation;
using Entities::Components::ComponentTypes;
#pragma endregion

#pragma region Usings
void Renderer::SetViewport(const Viewport& value)
{
	glViewport(value.GetTopLeft().GetX(),
			   value.GetTopLeft().GetY(),
		       value.GetSize().GetWidth(),
		       value.GetSize().GetHeight());
}
#pragma endregion

#pragma region Construction & Destruction
Renderer::Renderer(const Viewport & viewport, Scene & scene)
	:_Scene(scene),
	 _Shaders(new ShaderPipelineRepository()),
	 _SkyRenderer(new SkyRenderer(*_Shaders)),
	 _WorldRenderer(new WorldRenderer(*_Shaders))
{
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_STENCIL_TEST);
	//glEnable(GL_CULL_FACE);
	glEnable(GL_TEXTURE_2D);

	glStencilOp(GL_KEEP, GL_KEEP, GL_REPLACE);

	SetViewport(viewport);

	_SkyRenderer->SetCamera(scene.GetActiveCamera()->GetComponent<Camera>(ComponentTypes::Camera));
	_WorldRenderer->SetCamera(scene.GetActiveCamera()->GetComponent<Camera>(ComponentTypes::Camera));
	_WorldRenderer->SetSpotLights(&scene.GetSpotLights());
	_WorldRenderer->SetPointLights(&scene.GetPointLights());
	_WorldRenderer->SetAmbientLight(scene.GetAmbientLight());
}

Renderer::~Renderer()
{}
#pragma endregion


void Renderer::Update(float timeDelta)
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);

	DrawBackground();
	DrawForeground();
}

void Renderer::DrawBackground()
{
	_SkyRenderer->Begin();

	_Scene.GetBackground().GetComponent<Transformation>(ComponentTypes::Transformation)
						  ->SetPosition(_Scene.GetActiveCamera()
											  ->GetComponent<Transformation>(ComponentTypes::Transformation)->GetPosition());
	_Scene.GetBackground().ForEach([this](SceneObject* obj) { _SkyRenderer->Render(*obj); });

	_SkyRenderer->End();
}

void Renderer::DrawForeground()
{
	_WorldRenderer->Begin();	
	_Scene.GetForeground().ForEach([this](SceneObject* obj) { _WorldRenderer->Render(*obj); });
	_WorldRenderer->End();
}