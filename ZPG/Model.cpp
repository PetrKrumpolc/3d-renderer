#include "Model.h"
#include "Transformation.h"

#pragma region Usings
using std::string;
using std::vector;

using Entities::SceneObject;
using Entities::Components::IComponent;
using Entities::Components::Model;
using Entities::Components::ComponentTypes;
using Entities::Components::Transformation;
using Entities::SceneObject;

using Rendering::Objects::Material;
#pragma endregion

#pragma region Properties
const string Model::GetPath() const
{
	return _Path;
}

void Model::SetPath(const string& value)
{
	_Path = value;
}

vector<Material*> & Model::GetMaterials()
{
	return _Materials;
}

ComponentTypes Model::GetType() const noexcept
{
	return ComponentTypes::Model;
}

SceneObject& Model::GetObjectContainer()
{
	return *_Container;
}
#pragma endregion

#pragma region Construction & Destruction
Model::Model()
	: _Container(new SceneObject())
{
	_Container->SetName("Container");
	_Container->Attach(new Transformation());
}

Model::~Model()
{
	delete _Container;

	for (Material* material : _Materials)
		delete material;

	_Materials.clear();
}
void Model::OnOwnerChanged(SceneObject* oldOwner, SceneObject* newOwner)
{
	if (newOwner != nullptr)
		newOwner->Attach(_Container);
}
#pragma endregion