#include "Viewport.h"

#pragma region Usings
using Rendering::Viewport;

using Core::User::Point;
using Core::User::Size;
#pragma endregion


#pragma region Properties
const Point& Viewport::GetTopLeft() const
{
	return _TopLeft;
}

const Size& Viewport::GetSize() const
{
	return _Size;
}
#pragma endregion


Viewport::Viewport(const Point & topLeft, const Size & size)
	:_TopLeft(topLeft),
	 _Size(size)
{}