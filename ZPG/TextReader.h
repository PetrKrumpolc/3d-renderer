#pragma once
#include <string>
#include <istream>

namespace Core::IO
{
	class TextReader
	{
	public:
		TextReader(std::istream * stream);
		~TextReader();

		std::string Read() const;

	private:
		std::istream* _Stream;
	};
}