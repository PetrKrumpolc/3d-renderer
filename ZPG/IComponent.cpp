#include "IComponent.h"

using Entities::SceneObject;
using Entities::Components::IComponent;


void IComponent::SetOwner(SceneObject* value)
{
	SceneObject* oldOwner = _Owner;
	_Owner = value;

	OnOwnerChanged(oldOwner, _Owner);
}
