#include "TextReader.h"

#include <sstream>

#pragma region Usings
using std::istream;
using std::string;
using std::stringstream;

using Core::IO::TextReader;
#pragma endregion

#pragma region Construction & Destruction
TextReader::TextReader(istream* stream)
	:_Stream(stream)
{}

TextReader::~TextReader()
{
	delete _Stream;
}
#pragma endregion


string TextReader::Read() const
{
	if (_Stream->fail())
		throw std::ios_base::failure(" File I/O Error ");

	stringstream buffer;
	buffer << _Stream->rdbuf();

	return buffer.str();
}
