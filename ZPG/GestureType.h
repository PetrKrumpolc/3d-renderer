#pragma once

namespace Core::User::Input::Gestures
{
	enum class GestureType
	{
		KeyStroke,
		MouseButton,
		MouseScroll,
		MouseMove
	};
}