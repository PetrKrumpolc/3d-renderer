#pragma once
#include <chrono>
#include <string>

namespace Core::Time
{
	class DateTime
	{
	public:
		static DateTime Now();

		std::string ToString() const;
		std::string ToString(const std::string & format) const;

	private:
		DateTime(std::chrono::time_point<std::chrono::system_clock> time);
		~DateTime() = default;

		std::chrono::time_point<std::chrono::system_clock> _Time;
	};
}