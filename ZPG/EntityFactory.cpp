#include "EntityFactory.h"
#include "Application.h"

#include "Transformation.h"
#include "Camera.h"
#include "PointLight.h"
#include "AmbientLight.h"
#include "SpotLight.h"
#include "Model.h"

#pragma region Usings
using Entities::EntityFactory;
using Entities::SceneObject;

using Entities::Components::ComponentTypes;
using Entities::Components::Transformation;
using Entities::Components::Camera;
using Entities::Components::AmbientLight;
using Entities::Components::PointLight;
using Entities::Components::SpotLight;
using Entities::Components::Model;

using glm::vec3;
#pragma endregion


SceneObject* EntityFactory::CreateCamera() const
{
	SceneObject* camera       = new SceneObject();
	Transformation* transform = new Transformation();
	Camera* camComponent      = new Camera(Application::GetInstance().GetMainWindow().GetSize(),
										  transform->GetPosition(),
		                                  transform->GetRotation().GetDirection(),
									      45,
									      0.1f,
									      1000.f);

	camera->SetName("Camera");
	camera->Attach(transform);
	camera->Attach(camComponent);

	Application::GetInstance().GetMainWindow().Resized.Subscribe(camComponent, &Camera::SetResolution);
	transform->PositionChanged.Subscribe(camComponent, &Camera::SetEye);
	transform->GetRotation().DirectionChanged.Subscribe(camComponent, &Camera::SetDirection);

	return camera;
}

SceneObject* EntityFactory::CreatePointLight() const
{
	SceneObject* point = new SceneObject();
	point->Attach(new Transformation());
	point->Attach(new PointLight());

	return point;
}

SceneObject* EntityFactory::CreatePointLight(vec3 diffuse, vec3 specular) const
{
	SceneObject* point = new SceneObject();
	point->Attach(new Transformation());
	point->Attach(new PointLight());

	point->GetComponent<PointLight>(ComponentTypes::PointLight)->SetDiffuseColor(diffuse);
	point->GetComponent<PointLight>(ComponentTypes::PointLight)->SetSpecularColor(specular);

	return point;
}

SceneObject* EntityFactory::CreateSpotLight() const
{
	SceneObject* spotObject = new SceneObject();
	spotObject->Attach(new Transformation());
	spotObject->Attach(new SpotLight());

	return spotObject;
}

SceneObject* EntityFactory::CreateAmbientLight() const
{
	SceneObject* spot = new SceneObject();
	spot->Attach(new Transformation());
	spot->Attach(new AmbientLight());

	return spot;
}

SceneObject* EntityFactory::CreateModel() const
{
	SceneObject* model = new SceneObject();

	model->Attach(new Transformation());
	model->Attach(new Model());

	return model;
}
