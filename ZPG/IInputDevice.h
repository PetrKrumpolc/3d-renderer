#pragma once
#include "InputType.h"
#include "IGesture.h"

namespace Core::User::Input
{
	class IInputDevice
	{
	public:
		virtual ~IInputDevice() {}

		virtual Core::User::Input::InputType GetType() const = 0;

		virtual void Flush() = 0;

		virtual bool IsActive(const Core::User::Input::Gestures::IGesture & gesture) const = 0;
	};
}