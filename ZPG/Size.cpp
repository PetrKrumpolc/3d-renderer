#include "Size.h"

#pragma region Usings
using Core::User::Size;
#pragma endregion

#pragma region Properties
int Size::GetWidth() const
{
	return _Width;
}

int Size::GetHeight() const
{
	return _Height;
}
#pragma endregion


Size::Size(int width, int height)
	:_Width(width),
	 _Height(height)
{}


Size& Size::operator=(const Size& size)
{
	_Width  = size._Width;
	_Height = size._Height;

	return *this;
}
