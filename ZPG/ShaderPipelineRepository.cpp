#include "ShaderPipelineRepository.h"
#include <algorithm>

#pragma region Usings
using Rendering::Shaders::ShaderPipelineRepository;
using Rendering::Shaders::ShaderPipeline;
using Rendering::Shaders::ShaderPipelineTypes;
#pragma endregion


ShaderPipelineRepository::~ShaderPipelineRepository()
{
	std::for_each(_Pipelines.begin(), _Pipelines.end(), [](ShaderPipeline* pipeline) { delete pipeline; });
}

void ShaderPipelineRepository::Preload()
{
	//LetCreatePipeline(ShaderPipelineTypes::Basic);
	LetCreatePipeline(ShaderPipelineTypes::LambertPhong);
	LetCreatePipeline(ShaderPipelineTypes::Sky);
}



ShaderPipeline & ShaderPipelineRepository::GetItem(ShaderPipelineTypes type)
{
	ShaderPipeline * pipeline = FindPipeline(type);

	if (pipeline != nullptr)
		return *pipeline;
	else
		return LetCreatePipeline(type);
}

ShaderPipeline* ShaderPipelineRepository::FindPipeline(ShaderPipelineTypes type)
{
	for (ShaderPipeline* pipeline : _Pipelines)
		if (pipeline->GetType() == type)
			return pipeline;
	
	return nullptr;
}

ShaderPipeline & ShaderPipelineRepository::LetCreatePipeline(ShaderPipelineTypes type)
{
	ShaderPipeline* pipeline = _PipelineFactory.Create(type);

	_Pipelines.push_front(pipeline);

	return *pipeline;
}
