#include "MoveCommand.h"

#include <glm/gtc/type_ptr.hpp>

#pragma region Usings
using Input::Commands::MoveCommand;
using Input::Commands::MoveType;

using Entities::Scene;
using Entities::Components::Transformation;
using Entities::Components::ComponentTypes;

using std::function;
using std::unordered_map;

using glm::vec3;
#pragma endregion

#pragma region Static
unordered_map<MoveType, function<void(float step, Transformation& transform)>> MoveCommand::_Movements = {
	{ MoveType::Forward, 
	  [](float step, Transformation& transform) { transform.SetPosition(transform.GetPosition() + (transform.GetRotation().GetDirection() * step)); }
	},
	{ MoveType::Backward,
	  [](float step, Transformation& transform) { transform.SetPosition(transform.GetPosition() - (transform.GetRotation().GetDirection() * step)); }
	},
	{ MoveType::Left,
	  [](float step, Transformation& transform) 
      { 
		transform.SetPosition(transform.GetPosition() - (glm::normalize(glm::cross(transform.GetRotation().GetDirection(), vec3(0.0f, 1.0f, 0.0f))) * step)); 
      }
	},
	{ MoveType::Right,
	  [](float step, Transformation& transform)
	  {
		transform.SetPosition(transform.GetPosition() + (glm::normalize(glm::cross(transform.GetRotation().GetDirection(), vec3(0.0f, 1.0f, 0.0f))) * step));
	  }
	}
};
#pragma endregion


#pragma region Construction & Destruction
MoveCommand::MoveCommand(Entities::Scene& scene, float step, MoveType moveType)
	:_Scene(scene),
	 _Step(step),
	 _MoveType(moveType)
{}

MoveCommand::~MoveCommand()
{}
#pragma endregion


void MoveCommand::Execute(float timeDelta)
{
	if (_Scene.GetActiveCamera() != nullptr && _Scene.GetActiveCamera()->HasComponent(ComponentTypes::Transformation))
		_Movements[_MoveType](timeDelta * _Step, *_Scene.GetActiveCamera()->GetComponent<Transformation>(ComponentTypes::Transformation));
}
