#include "Mouse.h"
#include "MouseButtonGesture.h"
#include "Window.h"

#pragma region Usings
using Core::User::Input::Mouse;
using Core::User::Input::InputType;
using Core::User::Input::Gestures::IGesture;
using Core::User::Input::Gestures::GestureType;
using Core::User::Input::Gestures::MouseButtonGesture;

using Core::User::Point;
using Core::User::Window;
#pragma endregion

#pragma region Properties
const Point& Mouse::GetPosition() const
{
	return _Position;
}
const Point Mouse::GetPositionDelta() const
{
	return _Position - _PrevPosition;
}
const Point& Mouse::GetScrollOffset() const
{
	return _ScrollOffset;
}


bool Mouse::IsPressed(int buttonId) const
{
	return _IsPressed[buttonId];
}

bool Mouse::IsScrolled() const
{
	return _PrevScrollOffset != _ScrollOffset;
}

bool Mouse::IsMoved() const
{
	return _PrevPosition != _Position;
}


InputType Mouse::GetType() const
{
	return InputType::Mouse;
}

bool Mouse::IsActive(const IGesture& gesture) const
{
	switch (gesture.GetType())
	{
	case GestureType::MouseButton:
		return IsPressed(reinterpret_cast<const MouseButtonGesture&>(gesture).GetButton());
	case GestureType::MouseMove:
		return IsMoved();
	case GestureType::MouseScroll:
		return IsScrolled();
	default:
		return false;
	}
}
#pragma endregion

#pragma region Construction & Destruction
Mouse::Mouse(GLFWwindow& owner)
	:_Owner(owner),
	 _Position(Point(0,0)),
	 _ScrollOffset(Point(0,0)),
	 _PrevPosition(Point(0,0)),
	 _PrevScrollOffset(Point(0,0))
{
	glfwSetCursorPosCallback(&owner, [](GLFWwindow* wnd, double x, double y) 
		{ 
			reinterpret_cast<Window*>(glfwGetWindowUserPointer(wnd))->GetMouse().OnMove(x, y);
		});
	glfwSetMouseButtonCallback(&owner, [](GLFWwindow* wnd, int btn, int action, int mode) 
		{ 
			reinterpret_cast<Window*>(glfwGetWindowUserPointer(wnd))->GetMouse().OnClick(btn, action, mode);
		});
	glfwSetScrollCallback(&owner, [](GLFWwindow* wnd, double x, double y) 
		{
			reinterpret_cast<Window*>(glfwGetWindowUserPointer(wnd))->GetMouse().OnScroll(x, y);
		});

	for (int i = 0; i < 8; i++)
		_IsPressed[i] = false;
}

Mouse::~Mouse()
{
	glfwSetCursorPosCallback(&_Owner, nullptr);
	glfwSetMouseButtonCallback(&_Owner, nullptr);
	glfwSetScrollCallback(&_Owner, nullptr);
}
#pragma endregion


void Mouse::OnMove(double x, double y)
{
	_Position     = Point(static_cast<int>(x), static_cast<int>(y));
}

void Mouse::OnClick(int button, int action, int mode)
{
	_IsPressed[button] = action == GLFW_PRESS;
}

void Mouse::OnScroll(double xOffset, double yOffset)
{
	_ScrollOffset     = Point(static_cast<int>(xOffset), static_cast<int>(yOffset));
}


void Mouse::Flush()
{
	_PrevPosition     = _Position;
	_PrevScrollOffset = _ScrollOffset;
}
