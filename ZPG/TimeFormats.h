#pragma once
#include <string>

namespace Core::Time::TimeFormats
{
	const static std::string HHMMSS = "%X";
	const static std::string FULL   = "%Y %m %d %X";
}