#pragma once
#include <string>

namespace Core::User
{
	class Point
	{
	public:
		Point(int x, int y);
		~Point() = default;

		int GetX() const;
		int GetY() const;

		Point & operator =(const Point& point);

		friend bool operator ==(const Point& lhs, const Point& rhs);
		friend bool operator !=(const Point& lhs, const Point& rhs);
		friend Point operator -(const Point& lhs, const Point& rhs);
		friend Point operator +(const Point& lhs, const Point& rhs);

		std::string ToString() const;
	private:
		int _X;
		int _Y;
	};
}