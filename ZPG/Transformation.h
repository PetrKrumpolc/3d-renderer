#pragma once
#include "IComponent.h"
#include "Rotation.h"
//#include "Event.h"

#include <glm/mat4x4.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtc/matrix_transform.hpp>

namespace Entities::Components
{
	class Transformation : public IComponent
	{
	public:
		Transformation();
		virtual ~Transformation() override;

		Entities::Components::ComponentTypes GetType() const noexcept override;


		const glm::vec3 & GetPosition() const noexcept;
		void SetPosition(const glm::vec3 & value) noexcept;

		Entities::Components::Rotation & GetRotation() noexcept;

		const glm::vec3 & GetScale() const noexcept;
		void SetScale(const glm::vec3 & value) noexcept;

		const glm::mat4 & GetMatrix() noexcept;

		Core::Events::Event<const glm::vec3 &> PositionChanged;
		Core::Events::Event<const glm::mat4 &> TransformationChanged;
	private:
		void UpdateMatrix();

		glm::vec3					   _Position;
		Entities::Components::Rotation _Rotation;
		glm::vec3					   _Scale;

		glm::mat4 _Matrix;
		glm::mat4 _ParentMatrix;

		Core::Events::EventInvoker<const glm::vec3 &> _OnPositionChanged;
		Core::Events::EventInvoker<const glm::mat4 &> _OnTransformationChanged;


		void OnDirectionChanged(const glm::vec3 &);
		void OnParentTransformChanged(const glm::mat4& matrix);
		void OnParentChanged(SceneObject * oldParent, SceneObject * newParent);

	protected:
		virtual void OnOwnerChanged(Entities::SceneObject* oldOwner, Entities::SceneObject* newOwner) override;
	};
}