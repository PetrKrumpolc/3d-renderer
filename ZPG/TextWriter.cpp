#include "TextWriter.h"

#include <ostream>

#pragma region Usings
using Core::IO::TextWriter;

using std::string;
using std::stringstream;
using std::endl;
using std::ostream;
#pragma endregion

#pragma region Construction & Destruction
TextWriter::TextWriter(ostream * stream)
	:_Stream(stream)
{
	if (stream->fail())
		throw std::ios_base::failure(" File I/O Error ");
}

TextWriter::~TextWriter()
{
	delete _Stream;
}
#pragma endregion


void TextWriter::Write(const string & text)
{
	(*_Stream) << text << endl;
}

void TextWriter::Write(const stringstream & stream)
{
	(*_Stream) << stream.rdbuf() << endl;
}
