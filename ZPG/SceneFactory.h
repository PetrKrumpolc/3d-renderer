#pragma once
#include "Scene.h"
#include "PrimitivesFactory.h"
#include "EntityFactory.h"

#include <string>

namespace Entities
{
	class SceneFactory
	{
	public:
		SceneFactory()  = default;
		~SceneFactory() = default;

		Entities::Scene* CreateSpheres() const;
		Entities::Scene* CreateMix() const;
		Entities::Scene* CreatePlane() const;
		Entities::Scene* FromModel(const std::string& path);
		Entities::Scene* CreateCubeTest() const;
	private:
		Rendering::Objects::PrimitivesFactory _PrimitivesFactory;
		Entities::EntityFactory				  _EntityFactory;
	};
}