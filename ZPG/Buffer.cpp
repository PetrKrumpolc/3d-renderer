#include "Buffer.h"

#pragma region Usings
using Rendering::Objects::Buffer;
#pragma endregion


size_t Buffer::GetSize() const
{
	return _Size;
}


#pragma region Construction & Destruction
Buffer::Buffer(const void* data, size_t size, GLenum type, GLenum usage)
	: _Type(type),
	  _Usage(usage),
	  _Data(data),
	  _Size(size)
{
	glGenBuffers(1, &_Id);
	glBindBuffer(type, _Id);
	glBufferData(type, size, data, usage);
}

Buffer::~Buffer()
{
	glDeleteBuffers(1, &_Id);

	delete[] _Data;
}
#pragma endregion


void Buffer::Bind()
{
	glBindBuffer(_Type, _Id);
}