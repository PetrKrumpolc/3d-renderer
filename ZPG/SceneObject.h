#pragma once
#include "IComponent.h"
#include "Event.h"

#include <forward_list>
#include <string>
#include <functional>

namespace Entities
{
	class SceneObject
	{
	public:
		SceneObject();
		~SceneObject();

		const std::string & GetName() const;
		void SetName(const std::string & value);

		unsigned char GetId() const;
		void SetId(unsigned char value);

		SceneObject* GetParent();

		void Attach(Entities::SceneObject*			  child);
		void Attach(Entities::Components::IComponent* component);

		bool HasComponent(Entities::Components::ComponentTypes type) const noexcept;

		template<typename T>
		T* GetComponent(Entities::Components::ComponentTypes type) noexcept
		{
			for (Entities::Components::IComponent* component : _Components)
				if (component->GetType() == type)
					return reinterpret_cast<T*>(component);

			return nullptr;
		}

		void ForEach(std::function<void(Entities::SceneObject*)> action);

		const std::string& ToString();


		Core::Events::Event<SceneObject*, SceneObject*> ParentChanged;
	private:
		std::string _Name;
		unsigned char _Id;

		Entities::SceneObject* _Parent;

		std::forward_list<Entities::SceneObject*>			  _Children;
		std::forward_list<Entities::Components::IComponent*>  _Components;

		Core::Events::EventInvoker<SceneObject*, SceneObject*> _OnParentChanged;
	};
}