#include "FileLoggerDecorator.h"
#include "LogTypes.h"
#include "TimeFormats.h"
#include "TextWriter.h"

#include <fstream>
#include <sstream>

#pragma region Usings
using Core::Debug::FileLoggerDecorator;
using Core::IO::TextWriter;

using std::string;
using std::stringstream;
using std::unique_ptr;
using std::ofstream;
using std::ios;
using std::endl;

using std::for_each;
#pragma endregion


#pragma region Construction & Destruction
FileLoggerDecorator::FileLoggerDecorator(string path, ILogger * logger)
	:_CacheLimit(25),
	 _CacheSize(0),
	 _Path(path),
	 _Logger(std::move(logger))
{}

FileLoggerDecorator::~FileLoggerDecorator()
{
	if(_CacheSize > 0)
		TrySaveToFile();

	delete _Logger;
}
#pragma endregion

#pragma region ILogger
void FileLoggerDecorator::LogError(const string & message)
{
	_Logger->LogError(message);
	AfterLogging(FormatLogMessage(LogTypes::ERROR_TYPE, message, Time::TimeFormats::FULL));
}

void FileLoggerDecorator::LogInfo(const string & message)
{
	_Logger->LogInfo(message);
	AfterLogging(FormatLogMessage(LogTypes::INFO_TYPE, message, Time::TimeFormats::FULL));
}

void FileLoggerDecorator::LogWarning(const string & message)
{
	_Logger->LogWarning(message);
	AfterLogging(FormatLogMessage(LogTypes::WARNING_TYPE, message, Time::TimeFormats::FULL));
}

void FileLoggerDecorator::LogDebug(const string & message)
{
#ifdef LOG_DEBUG
	_Logger->LogDebug(message);
	AfterLogging(FormatLogMessage(LogTypes::DEBUG_TYPE, message, Time::TimeFormats::FULL));
#endif // LOG_DEBUG
}

void FileLoggerDecorator::Flush()
{
	_Logger->Flush();
	TrySaveToFile();
}
#pragma endregion

#pragma region Cache
void FileLoggerDecorator::ResetCache()
{
	_CacheSize = 0;
	_Cache.clear();
}

void FileLoggerDecorator::AppendToCache(const string& message)
{
	_Cache << message << endl;
	_CacheSize++;
}
#pragma endregion

void FileLoggerDecorator::AfterLogging(const string& message)
{
	AppendToCache(message);

	if (_CacheSize >= _CacheLimit)
	{
		TrySaveToFile();
		ResetCache();
	}
}

void FileLoggerDecorator::TrySaveToFile()
{
	try
	{
		TextWriter(new ofstream(_Path)).Write(_Cache);
		_Logger->LogInfo("Logged into: " + _Path);
	}
	catch (const std::ios_base::failure& e)
	{
		_Logger->LogError(e.what() + '-' + _Path);
	}
}
