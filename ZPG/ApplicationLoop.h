#pragma once
#include "Renderer.h"
#include "InputController.h"
#include "MovementSystem.h"
#include "Scene.h"
#include "FPSCounter.h"
#include "Size.h"

class ApplicationLoop
{
public:
	ApplicationLoop();
	~ApplicationLoop();

	void Initialize();

	void Start();

private:

	void ShowFPS() const;

	void OnWindowResized(const Core::User::Size & size);

	Rendering::Renderer*	_Renderer;
	Input::InputController* _Input;
	MovementSystem*			_Movement;

	Entities::Scene* _Scene;

	Core::FPSCounter _MainFPS;
	Core::FPSCounter _LogicFPS;
};