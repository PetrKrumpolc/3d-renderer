#pragma once
#include "IComponent.h"
#include <glm/gtc/type_ptr.hpp>

namespace Entities::Components
{
	class SpotLight : Entities::Components::IComponent
	{
	public:
		SpotLight();
		virtual ~SpotLight() override;

		virtual Entities::Components::ComponentTypes GetType() const noexcept override;

		const glm::vec3& GetColor() const;
		void SetColor(const glm::vec3& value);

	private:
		glm::vec3 _Color;
	};
}