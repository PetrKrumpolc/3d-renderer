#pragma once
#include "glm/vec3.hpp"
#include "BezierPath.h"

namespace Curves
{
	class BezierSpline
	{
	public:
		BezierSpline() = default;
		~BezierSpline() = default;

		glm::vec3 InterpolatePoint(Entities::Components::BezierPath & path);

	private:

		float Factorial(int number);
		float BinomialCoeficient(int n, int k);
		float BaseCoeficient(int pointCount, int pointIndex, float t);
	};
}