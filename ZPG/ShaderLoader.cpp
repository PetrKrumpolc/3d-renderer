#include "ShaderLoader.h"
#include "TextReader.h"

#include <fstream>

#pragma region Usings
using Core::IO::TextReader;

using Rendering::Shaders::Shader;
using Rendering::Shaders::ShaderLoader;
using Rendering::Shaders::ShaderTypes;

using std::string;
using std::ifstream;
#pragma endregion


Shader* ShaderLoader::TryLoad(const string & path) const
{
	try
	{
		return new Shader(TextReader(new ifstream(path)).Read(), GetType(path));
	}
	catch (const std::ios_base::failure&)
	{
		throw std::ios_base::failure(path);
	}
}

ShaderTypes ShaderLoader::GetType(const string & path) const
{
	string extension = path.substr(path.find_last_of(".") + 1);

	if (extension == "vs")
		return ShaderTypes::Vertex;
	else
		return ShaderTypes::Fragment;
}
