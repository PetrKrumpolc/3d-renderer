#include <cstdio>
#include <cstdlib>
#include <exception>

#include "Application.h"


int main() 
{
	try
	{
		Application::GetInstance().Initialize();
		Application::GetInstance().Run();
		exit(EXIT_SUCCESS);
	}
	catch (const std::exception & e)
	{
		Application::GetInstance().GetLogger().LogError(e.what());
		exit(EXIT_FAILURE);
	}
}