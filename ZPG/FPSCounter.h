#pragma once

namespace Core
{
	class FPSCounter
	{
	public:
		FPSCounter();
		~FPSCounter() = default;

		int GetFPS() const;

		void Update(double elapsed);

	private:

		void Reset();

		double _AccumulatedTime;
		int    _Frames;
		int    _FramesPerSecond;
	};
}