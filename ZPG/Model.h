#pragma once
#include "Material.h"
#include "SceneObject.h"
#include "IComponent.h"

#include <string>
#include <vector>

namespace Entities::Components
{
	class Model : public Entities::Components::IComponent
	{
	public:

		const std::string GetPath() const;
		void SetPath(const std::string & value);

		std::vector<Rendering::Objects::Material*> & GetMaterials();

		virtual Entities::Components::ComponentTypes GetType() const noexcept override;

		Entities::SceneObject& GetObjectContainer();


		Model();
		virtual ~Model() override;

	private:
		std::string _Path;

		std::vector<Rendering::Objects::Material*> _Materials;

		Entities::SceneObject* _Container;
	protected:
		virtual void OnOwnerChanged(Entities::SceneObject* oldOwner, Entities::SceneObject* newOwner) override;
	};
}