#include "ShaderPipelineFactory.h"

#include <stdexcept>

#pragma region Usings
using Rendering::Shaders::ShaderPipelineFactory;
using Rendering::Shaders::ShaderPipelineTypes;
using Rendering::Shaders::ShaderPipeline;

using std::string;
using std::runtime_error;
using std::map;
#pragma endregion

#pragma region Construction
ShaderPipelineFactory::ShaderPipelineFactory()
{
	_PipelineConfigs[ShaderPipelineTypes::Basic]        = { "../ZPG/basic.vs", "../ZPG/basic.fs" };
	_PipelineConfigs[ShaderPipelineTypes::LambertPhong] = { "../ZPG/LambertPhong.vs", "../ZPG/LambertPhong.fs" };
	_PipelineConfigs[ShaderPipelineTypes::Sky]			= { "../ZPG/Sky.vs", "../ZPG/Sky.fs" };
}
#pragma endregion


ShaderPipeline* ShaderPipelineFactory::Create(ShaderPipelineTypes type)
{
	return Construct(type, _PipelineConfigs[type]);
}

ShaderPipeline* ShaderPipelineFactory::Construct(ShaderPipelineTypes type, const PipelineFiles& files)
{
	ShaderPipeline* pipeline = new ShaderPipeline(type);

	Shader * vertex   = _Loader.TryLoad(files.vertex);
	Shader * fragment = _Loader.TryLoad(files.fragment);

	pipeline->Attach(*vertex);
	pipeline->Attach(*fragment);

	pipeline->Initialize();

	delete vertex;
	delete fragment;

	if (!pipeline->IsReady())
		throw runtime_error("Shader Pipeline is invalid. " + pipeline->GetInfo());

	return pipeline;
}
