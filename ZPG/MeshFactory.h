#pragma once
#include "Mesh.h"

namespace Rendering::Objects
{
	class ObjectFactory
	{
	public:
		ObjectFactory()  = default;
		~ObjectFactory() = default;

		Mesh* CreateTriangle() const;
	};
}