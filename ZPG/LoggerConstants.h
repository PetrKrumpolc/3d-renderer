#pragma once
#include <string>

namespace Core::Debug::LogTypes
{
	const static std::string ERROR_TYPE   = "Error";
	const static std::string INFO_TYPE    = "Info";
	const static std::string WARNING_TYPE = "Warning";
}