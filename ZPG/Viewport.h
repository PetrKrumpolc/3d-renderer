#pragma once
#include "Point.h"
#include "Size.h"

namespace Rendering
{
	class Viewport
	{
	public:
		Viewport(const Core::User::Point & topLeft, const Core::User::Size & size);
		~Viewport() = default;

		const Core::User::Point& GetTopLeft() const;
		const Core::User::Size&  GetSize()    const;

	private:
		const Core::User::Point _TopLeft;
		const Core::User::Size  _Size;
	};
}