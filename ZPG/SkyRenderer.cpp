#include "SkyRenderer.h"

#include "Mesh.h"
#include "Transformation.h"


#pragma region Usings
using Rendering::SkyRenderer;
using Rendering::RendererBase;
using Rendering::Shaders::ShaderPipelineRepository;
using Rendering::Shaders::ShaderPipelineTypes;
using Rendering::Objects::Material;
using Rendering::Objects::Mesh;

using Entities::SceneObject;
using Entities::Components::ComponentTypes;
using Entities::Components::Transformation;
using Entities::Components::Camera;
#pragma endregion


#pragma region Construction & Destruction
SkyRenderer::SkyRenderer(ShaderPipelineRepository& shaders)
	: RendererBase()
{
	_Program = &shaders.GetItem(ShaderPipelineTypes::Sky);
}

SkyRenderer::~SkyRenderer()
{
}
#pragma endregion


void SkyRenderer::Begin()
{
	_Program->Use();

	_Program->TrySetUniform("viewMatrix",		_Camera->GetViewMatrix());
	_Program->TrySetUniform("projectionMatrix", _Camera->GetProjectionMatrix());
}

void SkyRenderer::Render(SceneObject & entity)
{
	if (IsDrawable(entity))
	{
		_Program->TrySetUniform("modelMatrix", entity.GetComponent<Transformation>(ComponentTypes::Transformation)->GetMatrix());
		SetTextures(entity.GetComponent<Mesh>(ComponentTypes::Mesh)->GetMaterial());

		DrawMesh(entity);
	}
}

void SkyRenderer::End()
{
	glClear(GL_DEPTH_BUFFER_BIT);

	_Program->Unuse();
}


void SkyRenderer::SetTextures(Material* material)
{
	if (material->GetDiffuseMap() != nullptr)
		material->GetDiffuseMap()->Bind();
}
