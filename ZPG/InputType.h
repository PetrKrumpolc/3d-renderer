#pragma once

namespace Core::User::Input
{
	enum class InputType
	{
		Keyboard,
		Mouse
	};
}