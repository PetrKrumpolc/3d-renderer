#include "ApplicationLoop.h"
#include "Viewport.h"
#include "Point.h"
#include "Application.h"

#include "SceneFactory.h"
#include "EntityFactory.h"
#include "InputContextFactory.h"

#pragma region Usings
using Rendering::Renderer;
using Rendering::Viewport;

using Input::InputController;
using Input::InputContextFactory;

using Entities::EntityFactory;
using Entities::Scene;
using Entities::SceneObject;
using Entities::SceneFactory;

using Core::User::Point;
using Core::User::Size;
#pragma endregion

const int    FPS        = 60;
const double FRAME_TIME = 1.0 / FPS;

#pragma region Construction & Destruction
ApplicationLoop::ApplicationLoop()
{}

ApplicationLoop::~ApplicationLoop()
{
	delete _Renderer;
	delete _Input;
	delete _Scene;
}

void ApplicationLoop::Initialize()
{
	SceneObject* camera = EntityFactory().CreateCamera();

	_Scene = SceneFactory().FromModel("../Debug/Models/test.obj");
	_Scene->GetForeground().Attach(camera);
	_Scene->SetActiveCamera(camera);

	_Movement = new MovementSystem(_Scene);
	_Renderer = new Renderer(Viewport(Point(0, 0), Application::GetInstance().GetMainWindow().GetSize()), *_Scene);
	_Input    = new InputController(Application::GetInstance().GetMainWindow().GetMouse(), 
									Application::GetInstance().GetMainWindow().GetKeyboard());
	_Input->SetContext(InputContextFactory(_Scene).CreateContext());

	Application::GetInstance().GetMainWindow().Resized.Subscribe(this, &ApplicationLoop::OnWindowResized);
}
#pragma endregion


void ApplicationLoop::Start()
{
	double lastTime    = glfwGetTime();
	double currentTime = 0.0;
	double elapsed     = 0.0;

	double accumulatedTime = 0.0;
	while (!Application::GetInstance().GetMainWindow().ShouldBeClosed())
	{
		currentTime      = glfwGetTime();
		elapsed          = currentTime - lastTime;
		lastTime         = currentTime;
		accumulatedTime += elapsed;
		_MainFPS.Update(elapsed);

		ShowFPS();

		_Input->Update(static_cast<float>(elapsed));

		while (accumulatedTime >= FRAME_TIME)
		{
			_LogicFPS.Update(accumulatedTime);
			_Movement->Update(static_cast<float>(accumulatedTime));
			accumulatedTime -= elapsed;
		}

		_Renderer->Update(static_cast<float>(elapsed));

		Application::GetInstance().GetMainWindow().Refresh();
	}
}

void ApplicationLoop::ShowFPS() const
{
	Application::GetInstance().GetMainWindow()
							  .SetTitle("Main FPS: " + std::to_string(_MainFPS.GetFPS()) 
								       + " Logic FPS: " + std::to_string(_LogicFPS.GetFPS()));
}

void ApplicationLoop::OnWindowResized(const Size & size)
{
	_Renderer->SetViewport(Viewport(Point(0, 0), size));
}
