#include "Point.h"

#pragma region Usings
using Core::User::Point;

using std::string;
#pragma endregion


#pragma region Properties
int Point::GetX() const
{
	return _X;
}

int Point::GetY() const
{
	return _Y;
}
#pragma endregion


Point::Point(int x, int y)
	:_X(x),
	 _Y(y)
{}

#pragma region Operators
Point& Point::operator=(const Point& point)
{
	_X = point._X;
	_Y = point._Y;

	return *this;
}


bool Core::User::operator==(const Point& lhs, const Point& rhs)
{
	return lhs._X == rhs._X && lhs._Y == rhs._Y;;
}

bool Core::User::operator!=(const Point& lhs, const Point& rhs)
{
	return !(lhs == rhs);
}

Point Core::User::operator-(const Point& lhs, const Point& rhs)
{
	return Point(lhs._X - rhs._X, lhs._Y - rhs._Y);
}
Point Core::User::operator+(const Point& lhs, const Point& rhs)
{
	return Point(lhs._X + rhs._X, lhs._Y + rhs._Y);
}
#pragma endregion


string Point::ToString() const
{
	return "[" + std::to_string(_X) + ";" + std::to_string(_Y) + "]";
}
