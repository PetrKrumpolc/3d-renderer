#version 420

uniform mat4 modelMatrix;
uniform mat4 viewMatrix;
uniform mat4 projectionMatrix;

layout(location=0) in vec3 in_Position;
layout(location=1) in vec3 in_Normal;
layout(location=2) in vec2 in_UV;
layout(location=3) in vec2 in_Tangent;

out vec2 ex_uv;


void main() 
{
	 gl_Position      = (projectionMatrix * viewMatrix * modelMatrix) * vec4(in_Position, 1.0);
	 ex_uv            = vec2(in_UV.s, 1.0 - in_UV.t);
}