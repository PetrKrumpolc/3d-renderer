#pragma once
#include "ILogger.h"
#include "Window.h"
#include "GLInfo.h"
#include "ApplicationLoop.h"

class Application
{
public:
	static Application &  GetInstance();

	Core::Debug::ILogger& GetLogger() const;
	OpenGL::GLInfo&       GetGLInfo() const;
	Core::User::Window&   GetMainWindow();

	void Run();
	void Initialize();

private:
	Application();
	~Application();

	void InitLogger() const;

	OpenGL::GLInfo*		  _GLInfo;
	Core::Debug::ILogger* _Logger;
	Core::User::Window*   _MainWindow;
	ApplicationLoop*	  _Loop;

	static Application*   _Current;
};