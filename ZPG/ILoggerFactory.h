#pragma once
#include "ILogger.h"

namespace Core::Debug
{
	class ILoggerFactory
	{
	public:
		virtual ~ILoggerFactory() = default;

		virtual ILogger * CreateLogger() const = 0;
	};
}