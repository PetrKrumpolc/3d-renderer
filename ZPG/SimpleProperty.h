#pragma once

namespace Core
{
	template<typename T>
	class SimpleProperty
	{
	public:
		SimpleProperty() = default;
		SimpleProperty(const SimpleProperty<T>&) = delete;
		SimpleProperty(const T & value)
			: _Value(value)
		{}
		~SimpleProperty() = default;

		SimpleProperty<T>& operator = (SimpleProperty<T>&) = delete;
		inline SimpleProperty<T> & operator = (const T & right)
		{
			_Value = right;
			return *this;
		}

		inline operator T& ()     { return _Value; }
		inline T & operator () () { return _Value; }


	private:
		T _Value;
	};
}