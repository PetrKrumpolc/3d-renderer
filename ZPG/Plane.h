#pragma once
#include "Vertex.h"

namespace Rendering::Objects::Primitives
{
	int PlaneSize = 6;

	static Rendering::Objects::Vertex PlaneVerticies[6] = {
		{{1.0f, 0.0f, 1.0f},   {0.0f, 1.0f, 0.0f},   {0.0f, 0.0f}},
		{{1.0f, 0.0f,-1.0f},   {0.0f, 1.0f, 0.0f},   {1.0f, 0.0f}},
		{{-1.0f, 0.0f,-1.0f},   {0.0f, 1.0f, 0.0f},   {1.0f, 1.0f}},

		{{-1.0f, 0.0f, 1.0f},   {0.0f, 1.0f, 0.0f},   {0.0f, 1.0f} },
		{{1.0f, 0.0f, 1.0f},   {0.0f, 1.0f, 0.0f},   {0.0f, 0.0f} },
		{{-1.0f, 0.0f,-1.0f},   {0.0f, 1.0f, 0.0f},   {1.0f, 1.0f} }
	};

}