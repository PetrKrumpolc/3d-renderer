#pragma once
#include <GL\glew.h>

namespace Rendering::Objects
{
	class Buffer
	{
	public:

		size_t GetSize() const;

		Buffer(const void * data, size_t size, GLenum target, GLenum usage);
		~Buffer();

		void Bind();

	private:
		GLuint _Id;

		const void*		_Data;
		const size_t	_Size;

		const GLenum _Type;
		const GLenum _Usage;
	};
}