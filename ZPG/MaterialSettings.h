#pragma once

namespace Rendering::Objects
{
	enum class MaterialSettings : unsigned int
	{
		UseDiffuseMap = 1,
		UseNormalMap  = 2,
		UseSpecualMap = 4
	};
}