#pragma once
#include "ILogger.h"

#include <chrono>
#ifdef OS_WIN
#include <Windows.h>
#endif // OS_WIN

namespace Core::Debug
{
	class ConsoleLogger : public ILogger
	{
	public:
		ConsoleLogger();
		virtual ~ConsoleLogger() override;

		virtual void LogError(const std::string & message)   override;
		virtual void LogInfo(const std::string & message)    override;
		virtual void LogWarning(const std::string & message) override;
		virtual void LogDebug(const std::string & message)   override;

		virtual void Flush() override;

	private:
#ifdef OS_WIN
		HANDLE _Console;
#endif // OS_WIN

		void PrintMessage(const std::string & type, const std::string & message);
	};
}