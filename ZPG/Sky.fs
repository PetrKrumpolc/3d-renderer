#version 420

in vec2 ex_uv;

out vec4 frag_colour;

uniform sampler2D diffuseMap;


void main() 
{
    frag_colour = texture(diffuseMap, ex_uv);
}