#pragma once
#include "ShaderLoader.h"
#include "ShaderPipeline.h"
#include "ShaderPipelineTypes.h"

#include <map>

namespace Rendering::Shaders
{
	class ShaderPipelineFactory
	{
	public:
		ShaderPipelineFactory();
		~ShaderPipelineFactory() = default;

		Rendering::Shaders::ShaderPipeline* Create(Rendering::Shaders::ShaderPipelineTypes type);

	private:

		struct PipelineFiles
		{
			std::string vertex;
			std::string fragment;
		};

		Rendering::Shaders::ShaderPipeline* Construct(Rendering::Shaders::ShaderPipelineTypes type, const PipelineFiles& files);

		Rendering::Shaders::ShaderLoader								 _Loader;
		std::map<Rendering::Shaders::ShaderPipelineTypes, PipelineFiles> _PipelineConfigs;
	};
}