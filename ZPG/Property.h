#pragma once
#include <functional>

namespace Core
{
	template<typename T>
	class Property
	{
	public:
		Property()
		{
			_Setter = [](T & value) -> T& { return value; };
			_Getter = [](T & value) -> T& { return value; };
		}
		Property(const T & value)
			: Property(),
			  _Value(value)
		{}
		Property(const T & value, const std::function<T&(T&)> & setter, const std::function<T&(T&)> & getter)
			: _Value(value),
			  _Getter(getter),
			  _Setter(setter)
		{}
		Property(const Property<T>&) = delete;
		~Property() = default;

		Property<T>& operator = (const Property<T> & right) = delete;
		inline Property<T>& operator = (T & right)
		{
			_Value = _Setter(right);
			return *this;
		}

		inline operator T & () { return _Getter(_Value); }   // implicit conversion for assigning
		inline T & operator() () { return _Getter(_Value); } // functor for direct member call

	private:

		std::function<T&(T&)> _Setter;
		std::function<T&(T&)> _Getter;

		T _Value;
	};
}