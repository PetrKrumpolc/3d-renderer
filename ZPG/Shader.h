#pragma once
#include <GL\glew.h>
#include <string>

#include "ShaderTypes.h"
#include "ShaderPipeline.h"

namespace Rendering::Shaders
{
	class Shader
	{
		friend class Rendering::Shaders::ShaderPipeline;
	public:
		Shader(const std::string & src, ShaderTypes type);
		~Shader();

	private:
		const GLuint _Shader;
	};
}
