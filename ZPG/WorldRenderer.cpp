#include "WorldRenderer.h"

#include "ShaderPipelineTypes.h"
#include "Mesh.h"

#include "PointLight.h"
#include "SpotLight.h"
#include "AmbientLight.h"
#include "Transformation.h"

#pragma region Usings
using Rendering::WorldRenderer;
using Rendering::Objects::Material;
using Rendering::Objects::MaterialConfig;
using Rendering::Objects::Mesh;
using Rendering::Shaders::ShaderPipelineRepository;
using Rendering::Shaders::ShaderPipelineTypes;

using Entities::SceneObject;
using Entities::Components::ComponentTypes;
using Entities::Components::PointLight;
using Entities::Components::SpotLight;
using Entities::Components::AmbientLight;
using Entities::Components::Transformation;
using Entities::Components::Camera;

using std::vector;
#pragma endregion

#pragma region Properties
void WorldRenderer::SetPointLights(vector<SceneObject*>* value)
{
	_PointLights = value;
}
void WorldRenderer::SetSpotLights(vector<SceneObject*>* value)
{
	_SpotLights = value;
}
void WorldRenderer::SetAmbientLight(SceneObject* value)
{
	_AmbientLight = value;
}
#pragma endregion


#pragma region Construction & Destruction
WorldRenderer::WorldRenderer(ShaderPipelineRepository& shaders)
{
	_Program = &shaders.GetItem(ShaderPipelineTypes::LambertPhong);
}

WorldRenderer::~WorldRenderer()
{
}
#pragma endregion


void WorldRenderer::Begin()
{
	_Program->Use();

	_Program->TrySetUniform("viewMatrix",		_Camera->GetViewMatrix());
	_Program->TrySetUniform("projectionMatrix", _Camera->GetProjectionMatrix());
	_Program->TrySetUniform("eyePosition",		_Camera->GetEye());

	BindPointLights();
	BindSpotLights();
	_Program->TrySetUniform("ambientLight", _AmbientLight->GetComponent<AmbientLight>(ComponentTypes::AmbientLight)->GetAmbientColor());
}

void WorldRenderer::Render(SceneObject & entity)
{
	if (IsDrawable(entity))
	{
		_Program->TrySetUniform("modelMatrix", entity.GetComponent<Transformation>(ComponentTypes::Transformation)->GetMatrix());
		BindMaterial(entity.GetComponent<Mesh>(ComponentTypes::Mesh)->GetMaterial()->GetConfiguration());
		BindTextures(entity.GetComponent<Mesh>(ComponentTypes::Mesh)->GetMaterial());

		DrawMesh(entity);
	}
}

void WorldRenderer::End()
{
	_Program->Unuse();
}


void WorldRenderer::BindMaterial(MaterialConfig& material)
{
	_Program->TrySetUniform("material.Diffuse",		material.Diffuse);
	_Program->TrySetUniform("material.Specular",	material.Specular);
	_Program->TrySetUniform("material.Shininnes",	material.Shininnes);
	_Program->TrySetUniform("material.Ambient",		material.Ambient);
}

void WorldRenderer::BindTextures(Material* material)
{
	if (material->GetDiffuseMap() != nullptr)
	{
		_Program->TrySetUniform("diffuseMap", *material->GetDiffuseMap());
		material->GetDiffuseMap()->Bind();
	}
	if (material->GetSpecularMap() != nullptr)
	{
		_Program->TrySetUniform("specularMap", *material->GetSpecularMap());
		material->GetSpecularMap()->Bind();
	}
	if (material->GetNormalMap() != nullptr)
	{
		_Program->TrySetUniform("normalMap", *material->GetNormalMap());
		material->GetNormalMap()->Bind();
	}

	_Program->TrySetUniform("materialSettings", static_cast<unsigned int>(material->GetSettings()));
}

void WorldRenderer::BindSpotLights()
{
	_Program->TrySetUniform("spotLightCount", static_cast<int>(_SpotLights->size()));

	for (unsigned int i = 0; i < _SpotLights->size(); i++)
		_Program->TrySetUniform("spotLights[" + std::to_string(i) + "]",
								*_SpotLights->at(i)->GetComponent<SpotLight>(ComponentTypes::SpotLight),
								*_SpotLights->at(i)->GetComponent<Transformation>(ComponentTypes::Transformation));
}

void WorldRenderer::BindPointLights()
{
	_Program->TrySetUniform("pointLightCount", static_cast<int>(_PointLights->size()));

	for (unsigned int i = 0; i < _PointLights->size(); i++)
		_Program->TrySetUniform("pointLights[" + std::to_string(i) + "]",
								*_PointLights->at(i)->GetComponent<PointLight>(ComponentTypes::PointLight),
								*_PointLights->at(i)->GetComponent<Transformation>(ComponentTypes::Transformation));
}
