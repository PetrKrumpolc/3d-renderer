#pragma once
#include "RendererBase.h"
#include "SceneObject.h"
#include "Material.h"
#include "ShaderPipeline.h"
#include "ShaderPipelineRepository.h"
#include "Camera.h"

#include <vector>

namespace Rendering
{
	class WorldRenderer : public Rendering::RendererBase
	{
	public:
		void SetPointLights(std::vector<Entities::SceneObject*> * value);
		void SetSpotLights(std::vector<Entities::SceneObject*> * value);
		void SetAmbientLight(Entities::SceneObject * value);

		WorldRenderer(Rendering::Shaders::ShaderPipelineRepository & shaders);
		~WorldRenderer();

		virtual void Begin() override;
		virtual void Render(Entities::SceneObject & entity) override;
		virtual void End() override;

	private:
		Rendering::Shaders::ShaderPipeline * _Program;

		std::vector<Entities::SceneObject*>* _PointLights;
		std::vector<Entities::SceneObject*>* _SpotLights;
		Entities::SceneObject*				 _AmbientLight;

		void BindMaterial(Rendering::Objects::MaterialConfig& material);
		void BindTextures(Rendering::Objects::Material* material);
		void BindSpotLights();
		void BindPointLights();
	};
}