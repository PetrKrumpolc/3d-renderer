#include "Shader.h"

#pragma region Usings
using Rendering::Shaders::Shader;
using Rendering::Shaders::ShaderTypes;

using std::string;
#pragma endregion

#pragma region Construction & Destruction
Shader::Shader(const string & src, ShaderTypes type)
	:_Shader(glCreateShader(static_cast<int>(type)))
{
	const char* source = src.c_str();

	glShaderSource(_Shader, 1, &source, NULL);
	glCompileShader(_Shader);
}

Shader::~Shader()
{
	glDeleteShader(_Shader);
}
#pragma endregion