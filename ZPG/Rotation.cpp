#include "Rotation.h"

#include <glm/gtc/matrix_transform.hpp>


#pragma region Usings
using Entities::Components::Rotation;

using Core::Events::Event;

using glm::vec3;
using glm::mat4;
#pragma endregion


#pragma region Properties
float Rotation::GetYaw() const
{
	return _Yaw;
}

void Rotation::SetYaw(float value)
{
	_Yaw = value;
	OnRotationChanged();
}

float Rotation::GetPitch() const
{
	return _Pitch;
}

void Rotation::SetPitch(float value)
{
	_Pitch = value;
	OnRotationChanged();
}

float Rotation::GetRoll() const
{
	return _Roll;
}

void Rotation::SetRoll(float value)
{
	_Roll = value;
	OnRotationChanged();
}

const mat4 & Rotation::GetRotationMatrix() const
{
	return _RotationMatrix;
}

const vec3 & Rotation::GetDirection() const
{
	return _Direction;
}
#pragma endregion

#pragma region Construction & Destruction
Rotation::Rotation()
	: _Yaw(0.0f),
	  _Pitch(0.0f),
	  _Roll(0.0f),
	 DirectionChanged(Event<const vec3&>(&_OnDirectionChanged))
{
	OnRotationChanged();
}

Rotation::~Rotation()
{
}
#pragma


void Rotation::OnRotationChanged()
{
	_Direction      = ComputeDirection(_Yaw, _Pitch);
	_RotationMatrix = ComputeRotationMatrix(_Yaw, _Pitch, _Roll);

	_OnDirectionChanged(_Direction);
}


vec3 Rotation::ComputeDirection(float yaw, float pitch) const
{
	return normalize(vec3(cos(glm::radians(yaw)) * cos(glm::radians(pitch)),
						  sin(glm::radians(pitch)),
						  sin(glm::radians(yaw)) * cos(glm::radians(pitch))));
}

mat4 Rotation::ComputeRotationMatrix(float yaw, float pitch, float roll) const
{
	mat4 matrix = mat4(1.0f);

	matrix = glm::rotate(matrix, glm::radians(_Pitch), vec3(1.0f, 0.0f, 0.0f));
	matrix = glm::rotate(matrix, glm::radians(_Yaw),   vec3(0.0f, 1.0f, 0.0f));
	matrix = glm::rotate(matrix, glm::radians(_Roll),  vec3(0.0f, 0.0f, 1.0f));

	return matrix;
}
