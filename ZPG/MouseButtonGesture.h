#pragma once
#include "IGesture.h"

namespace Core::User::Input::Gestures
{
	class MouseButtonGesture : public Core::User::Input::Gestures::IGesture
	{
	public:
		MouseButtonGesture(int button);
		virtual ~MouseButtonGesture() override;

		int GetButton() const;

	private:
		const int _Button;
	};
}