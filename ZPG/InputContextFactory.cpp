#include <GL/glew.h>
#include <GLFW\glfw3.h>

#include "InputContextFactory.h"
#include "Action.h"
#include "KeyboadGesture.h"
#include "MouseButtonGesture.h"
#include "Application.h"

#include "MoveCommand.h"
#include "RotateCommad.h"
#include "CloseCommand.h"
#include "SelectCommand.h"

#pragma region Usings
using Input::InputContextFactory;
using Input::InputContext;

using Input::Action;
using Input::ActionType;

using Input::Commands::CloseCommand;
using Input::Commands::MoveCommand;
using Input::Commands::RotateComamnd;
using Input::Commands::SelectCommand;
using Input::Commands::MoveType;

using Core::User::Input::Gestures::KeyboardGesture;
using Core::User::Input::Gestures::MouseButtonGesture;

using Entities::Scene;
#pragma endregion


InputContextFactory::InputContextFactory(Scene* scene)
	:_Scene(scene)
{}

InputContext* InputContextFactory::CreateContext()
{
	InputContext* context = new InputContext();

	context->Add(new Action(ActionType::Close,		  new KeyboardGesture(GLFW_KEY_ESCAPE), new CloseCommand(Application::GetInstance())));
	context->Add(new Action(ActionType::MoveForward,  new KeyboardGesture(GLFW_KEY_W),      new MoveCommand(*_Scene, 2, MoveType::Forward)));
	context->Add(new Action(ActionType::MoveBackward, new KeyboardGesture(GLFW_KEY_S),      new MoveCommand(*_Scene, 2, MoveType::Backward)));
	context->Add(new Action(ActionType::MoveLeft,     new KeyboardGesture(GLFW_KEY_A),      new MoveCommand(*_Scene, 2, MoveType::Left)));
	context->Add(new Action(ActionType::MoveRight,    new KeyboardGesture(GLFW_KEY_D),      new MoveCommand(*_Scene, 2, MoveType::Right)));

	context->Add(new Action(ActionType::Rotate, new MouseButtonGesture(GLFW_MOUSE_BUTTON_LEFT), new RotateComamnd(Application::GetInstance().GetMainWindow().GetMouse(), *_Scene, 180)));
	context->Add(new Action(ActionType::Select, new MouseButtonGesture(GLFW_MOUSE_BUTTON_RIGHT), new SelectCommand(Application::GetInstance().GetMainWindow(), *_Scene)));

	return context;
}
