#pragma once
#include <string>

#include "Shader.h"
#include "ShaderTypes.h"

namespace Rendering::Shaders
{
	class ShaderLoader
	{
	public:
		ShaderLoader()  = default;
		~ShaderLoader() = default;

		Shader* TryLoad(const std::string & path) const;

	private:
		ShaderTypes GetType(const std::string & path) const;
	};
}