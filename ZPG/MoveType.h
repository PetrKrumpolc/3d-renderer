#pragma once

namespace Input::Commands
{
	enum class MoveType
	{
		Forward,
		Backward,
		Left,
		Right
	};
}