#pragma once
#include <string>

namespace OpenGL
{
	class GLInfo
	{
	public:
		GLInfo();
		~GLInfo() = default;

		const std::string & GetOpenGLVersion()   const;
		const std::string & GetRendererVersion() const;

		const std::string & GetGLEWVersion() const;
		const std::string & GetGLSLVersion() const;
		const std::string & GetGLFWVersion() const;

		const std::string & GetGPUVendor() const;

	private:
		std::string _OpenGLVersion;
		std::string _RendererVersion;

		std::string _GLEWVersion;
		std::string _GLSLVersion;
		std::string _GLFWVersion;

		std::string _GPUVendor;
	};
}