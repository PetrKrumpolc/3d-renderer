#include "RendererBase.h"

#include "ComponentTypes.h"
#include "Mesh.h"

#pragma region Usings
using Rendering::RendererBase;
using Rendering::Objects::Mesh;

using Entities::SceneObject;
using Entities::Components::ComponentTypes;
using Entities::Components::Camera;
#pragma endregion



Camera* RendererBase::GetCamera()
{
	return _Camera;
}

void RendererBase::SetCamera(Camera* value)
{
	_Camera = value;
}



void RendererBase::DrawMesh(SceneObject & entity)
{
	Mesh* mesh = entity.GetComponent<Mesh>(ComponentTypes::Mesh);

	mesh->Bind();

	glStencilFunc(GL_ALWAYS, entity.GetId(), 0xFF);

	//glDrawArrays(GL_TRIANGLES, 0, mesh->GetVertexCount());
	glDrawElements(GL_TRIANGLES, mesh->GetIndexCount(), GL_UNSIGNED_INT, NULL);
}

bool RendererBase::IsDrawable(SceneObject & entity)
{
	return entity.HasComponent(ComponentTypes::Transformation) && entity.HasComponent(ComponentTypes::Mesh);
}
