#include "PointLight.h"

#pragma region Usings
using Entities::Components::IComponent;
using Entities::Components::PointLight;
using Entities::Components::ComponentTypes;

using glm::vec3;
#pragma endregion

#pragma region Construction & Destruction
PointLight::PointLight()
	: _DiffuseColor(vec3(0.385f, 0.647f, 0.812f)),
	  _SpecularColor(vec3(1.0f, 1.0f, 1.0f)),
	  _Shininess(128),
	  _Constant(1.0f),
	  _Linear(0.09f),
	  _Quadratic(0.032f),
	  _Power(5.0f)
{
}

PointLight::~PointLight()
{
}
#pragma endregion


ComponentTypes PointLight::GetType() const noexcept
{
	return ComponentTypes::PointLight;
}

float PointLight::GetPower() const
{
	return _Power;
}

void PointLight::SetPower(float value)
{
	_Power = value;
}


const vec3& PointLight::GetDiffuseColor() const
{
	return _DiffuseColor;
}

void PointLight::SetDiffuseColor(const vec3& value)
{
	_DiffuseColor = value;
}

const vec3& PointLight::GetSpecularColor() const
{
	return _SpecularColor;
}

void PointLight::SetSpecularColor(const vec3& value)
{
	_SpecularColor = value;
}


float PointLight::GetShininess() const
{
	return _Shininess;
}

void PointLight::SetShininess(float value)
{
	_Shininess = value;
}

float PointLight::GetConstant() const
{
	return _Constant;
}

void PointLight::SetConstant(float value)
{
	_Constant = value;
}

float PointLight::GetLinear() const
{
	return _Linear;
}

void PointLight::SetLinear(float value)
{
	_Linear = value;
}

float PointLight::GetQuadratic() const
{
	return _Quadratic;
}

void PointLight::SetQuadratic(float value)
{
	_Quadratic = value;
}
