#pragma once
#include "ISystem.h"
#include "Mouse.h"
#include "Keyboard.h"
#include "InputContext.h"
#include "IInputDevice.h"

#include <vector>

namespace Input
{
	class InputController : public Core::ISystem
	{
	public:
		InputController(Core::User::Input::Mouse& mouse, Core::User::Input::Keyboard& keyboard);
		~InputController();

		void SetContext(Input::InputContext* value);

		virtual void Update(float timeDelta) override;

	private:
		Input::InputContext* _Context;

		std::vector<Core::User::Input::IInputDevice*> _Devices;
	};
}