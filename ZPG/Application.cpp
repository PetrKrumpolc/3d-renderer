#include "Application.h"
#include "LoggerFactory.h"
#include "Size.h"

#include <GL/glew.h>
#include <GLFW/glfw3.h>

#include <stdexcept>

#pragma region Usings
using Core::User::Window;
using Core::User::Size;
using Core::User::Point;

using Core::Debug::ILogger;
using Core::Debug::LoggerFactory;

using OpenGL::GLInfo;
#pragma endregion

#pragma region Singleton
Application* Application::_Current;

Application & Application::GetInstance()
{
	if (Application::_Current == nullptr)
		Application::_Current = new Application();

	return *Application::_Current;
}
#pragma endregion

#pragma region Properties
ILogger& Application::GetLogger() const
{
	return *_Logger;
}

GLInfo& Application::GetGLInfo() const
{
	return *_GLInfo;
}

Window& Application::GetMainWindow()
{
	return *_MainWindow;
}
#pragma endregion

#pragma region Construction & Destruction & Initialization
Application::Application()
{
	_Logger = LoggerFactory().CreateLogger();
	_Loop   = new ApplicationLoop();
}

Application::~Application()
{
	delete _MainWindow;
	delete _Logger;
	delete _GLInfo;
	delete _Loop;
}


void Application::Initialize()
{
	glfwSetErrorCallback([](int error, const char* description) { Application::GetInstance().GetLogger().LogError(description); });

	if (!glfwInit())
		throw std::runtime_error("Could not start GLFW3");

	_MainWindow = new Window(Size(800, 600), "OpenGL");
	_MainWindow->SetVSync(true);

	glewExperimental = GL_TRUE;
	if(glewInit() != GLEW_OK)
		throw std::runtime_error("Could not start GLEW");

	_GLInfo = new GLInfo();

	InitLogger();
	_Loop->Initialize();
}

void Application::InitLogger() const
{
	GetLogger().LogInfo("OpenGL Version: " + GetGLInfo().GetOpenGLVersion());
	GetLogger().LogInfo("Vendor: "		   + GetGLInfo().GetGPUVendor());
	GetLogger().LogInfo("Renderer: "	   + GetGLInfo().GetRendererVersion());
	GetLogger().LogInfo("GLEW: "		   + GetGLInfo().GetGLEWVersion());
	GetLogger().LogInfo("GLSL: "		   + GetGLInfo().GetGLSLVersion());
	GetLogger().LogInfo("GLFW: "		   + GetGLInfo().GetGLFWVersion());
}
#pragma endregion

void Application::Run()
{
	_Loop->Start();
	GetLogger().Flush();
}