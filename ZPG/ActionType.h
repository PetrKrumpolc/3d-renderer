#pragma once

namespace Input
{
	enum class ActionType
	{
		MoveForward,
		MoveBackward,
		MoveLeft,
		MoveRight,
		Rotate,
		Close,
		Select
	};
}