#pragma once
#include "SceneObject.h"
#include "Model.h"
#include "Material.h"
#include "Texture.h"

#include <string>
#include <vector>

#include <assimp/scene.h>
#include <assimp/Importer.hpp>


namespace Import
{
	class ModelImporter
	{
	public:
		ModelImporter(const std::string& path);
		~ModelImporter();

		Entities::SceneObject* Import();

	private:
		Assimp::Importer* _Importer;
		const aiScene*    _ImportedScene;
		const std::string _Path;

		void ImportMaterials(Entities::Components::Model & model);
		void ImportObjects(Entities::Components::Model& model);

		Entities::SceneObject* ConvertObject(aiMesh * mesh, const std::vector<Rendering::Objects::Material*> & materials);
		Rendering::Objects::Material* ConvertMaterial(aiMaterial * material);

		glm::vec3 ImportColor(aiMaterial* material, const char * key, unsigned int type, unsigned int index);
		float ImportFloat(aiMaterial* material, const char* key, unsigned int type, unsigned int index);
		Rendering::Objects::Texture* ImportTexture(aiMaterial* material, aiTextureType type, Rendering::Objects::TextureTypes customType);
	};
}