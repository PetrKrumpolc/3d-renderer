#include "BezierSpline.h"

#pragma region Usings
using Curves::BezierSpline;

using glm::vec3;
#pragma endregion


vec3 BezierSpline::InterpolatePoint(Entities::Components::BezierPath& path)
{
	vec3 point(0.0f);

	float baseCoef;

	for (unsigned int i = 0; i < path.Points().size(); i++)
	{
		baseCoef = BaseCoeficient(path.Points().size() -1, i, path.Coeficient);
		point.x += path.Points().at(i).x * baseCoef;
		point.y += path.Points().at(i).y * baseCoef;
		point.z += path.Points().at(i).z * baseCoef;
	}

	return point;
}


float BezierSpline::Factorial(int number)
{
	return number == 0 ? 1 : number * Factorial(number - 1);
}

float BezierSpline::BinomialCoeficient(int n, int k)
{
	return Factorial(n) / (Factorial(n - k) * Factorial(k));
}

float BezierSpline::BaseCoeficient(int pointCount, int pointIndex, float t)
{
	return BinomialCoeficient(pointCount, pointIndex) * powf(t, static_cast<float>(pointIndex)) * powf(1.0f - t, static_cast<float>(pointCount - pointIndex));
}
