#include "MovementSystem.h"
#include "Transformation.h"
#include "Movement.h"

#pragma region Usings
using Entities::Scene;
using Entities::SceneObject;
using Entities::Components::BezierPath;
using Entities::Components::Transformation;
using Entities::Components::Movement;
using Entities::Components::ComponentTypes;

using glm::vec3;
#pragma endregion


MovementSystem::MovementSystem(Scene * scene)
	: _Scene(scene)
{}

void MovementSystem::Update(float timeDelta)
{
	_Scene->GetForeground().ForEach([this,&timeDelta](SceneObject* obj) { TryMove(obj, timeDelta); });
	
	for (SceneObject* point : _Scene->GetPointLights())
		TryMove(point, timeDelta);

	for (SceneObject* spot : _Scene->GetSpotLights())
		TryMove(spot, timeDelta);
}


void MovementSystem::TryMove(SceneObject* obj, float timeDelta)
{
	if (obj->HasComponent(ComponentTypes::Movement))
		obj->GetComponent<Transformation>(ComponentTypes::Transformation)->SetPosition(MoveAlong(*obj->GetComponent<BezierPath>(ComponentTypes::BezierCubicPath),
																								  obj->GetComponent<Movement>(ComponentTypes::Movement)->Velocity * timeDelta));
}

vec3 MovementSystem::MoveAlong(BezierPath& path, float velocity)
{
	float t = path.Coeficient + velocity;

	path.Coeficient = t > 1.0f ? 0.0f : t;
	return _Bezier.InterpolatePoint(path);
}
