#include "GLInfo.h"
#include <GL\glew.h>
#include <GLFW\glfw3.h>

#pragma region Usings
using OpenGL::GLInfo;

using std::string;
using std::to_string;
#pragma endregion


#pragma region Properties
const string & GLInfo::GetOpenGLVersion() const
{
	return _OpenGLVersion;
}

const string & GLInfo::GetRendererVersion() const
{
	return _RendererVersion;
}

const string & GLInfo::GetGLEWVersion() const
{
	return _GLEWVersion;
}

const string & GLInfo::GetGLSLVersion() const
{
	return _GLSLVersion;
}

const string & GLInfo::GetGLFWVersion() const
{
	return _GLFWVersion;
}

const string & GLInfo::GetGPUVendor() const
{
	return _GPUVendor;
}
#pragma endregion


#pragma region Construction
GLInfo::GLInfo()
	:_OpenGLVersion(string(reinterpret_cast<const char*>(glGetString(GL_VERSION)))),
	 _RendererVersion(string(reinterpret_cast<const char*>(glGetString(GL_RENDERER)))),
	 _GLEWVersion(string(reinterpret_cast<const char*>(glewGetString(GLEW_VERSION)))),
	 _GLSLVersion(string(reinterpret_cast<const char*>(glGetString(GL_SHADING_LANGUAGE_VERSION)))),
	 _GPUVendor(string(reinterpret_cast<const char*>(glGetString(GL_VENDOR))))
{
	int major, minor, revision;
	glfwGetVersion(&major, &minor, &revision);
	_GLFWVersion = to_string(major) + '.' + to_string(minor) + '.' + to_string(revision);
}
#pragma endregion
