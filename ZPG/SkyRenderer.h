#pragma once
#include "RendererBase.h"
#include "SceneObject.h"
#include "ShaderPipeline.h"
#include "ShaderPipelineRepository.h"
#include "Material.h"
#include "Camera.h"

namespace Rendering
{
	class SkyRenderer : public Rendering::RendererBase
	{
	public:
		SkyRenderer(Rendering::Shaders::ShaderPipelineRepository & shaders);
		virtual ~SkyRenderer() override;

		virtual void Begin() override;
		virtual void Render(Entities::SceneObject & entity) override;
		virtual void End() override;

	private:
		Rendering::Shaders::ShaderPipeline * _Program;

		void SetTextures(Rendering::Objects::Material* material);
	};
}