#pragma once
#include "ICommand.h"
#include "Window.h"
#include "Scene.h"

namespace Input::Commands
{
	class SelectCommand : public Input::Commands::ICommand
	{
	public:
		SelectCommand(Core::User::Window & window, Entities::Scene & scene);
		virtual ~SelectCommand() override;


		virtual void Execute(float timeDelta) override;

	private:
		Core::User::Window& _Window;
		Entities::Scene&	_Scene;
	};
}