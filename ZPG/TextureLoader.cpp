#include "TextureLoader.h"
#include "Application.h"

#include <SOIL.h>

#include <stdexcept>

#pragma region Usings
using Rendering::Objects::TextureLoader;
using Rendering::Objects::Texture;
using Rendering::Objects::TextureTypes;

using std::string;
#pragma endregion


Texture* TextureLoader::LoadTexture(const string & path, TextureTypes type) const
{
	int width;
	int height;
	int channels;

	uint8_t* image   = SOIL_load_image(path.c_str(), &width, &height, &channels, SOIL_LOAD_RGB);
	Texture* texture = nullptr;

	const char * result = SOIL_last_result();
	if (!image)
		throw std::runtime_error("Image not found - " + path);
	else
		Application::GetInstance().GetLogger().LogInfo(string(result) + " - " + path);

	texture = new Texture(path, type);
	texture->SetImage(width, height, image, GL_TEXTURE_2D);

	SOIL_free_image_data(image);

	return texture;
}
