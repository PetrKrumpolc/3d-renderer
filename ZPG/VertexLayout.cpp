#include "VertexLayout.h"

#pragma region Usings
using Rendering::Objects::VertexLayout;
#pragma endregion


#pragma region Construction & Destruction
VertexLayout::VertexLayout()
{
	glGenVertexArrays(1, &_Id);
}

VertexLayout::~VertexLayout()
{
	glDeleteVertexArrays(1, &_Id);
}
#pragma endregion


void VertexLayout::AddChunk(int index, size_t size, GLenum dataType, bool isNormalized, size_t stride, const void* offset)
{
	Bind();

	glVertexAttribPointer(index, size, dataType, isNormalized, stride, offset);
	glEnableVertexAttribArray(index);

	Unbind();
}


void VertexLayout::Bind()
{
	glBindVertexArray(_Id);
}

void VertexLayout::Unbind()
{
	glBindVertexArray(0);
}
