#pragma once
#include "Property.h"
#include "SceneObject.h"

#include <vector>

namespace Entities
{
	class Scene
	{
	public:
		Scene();
		~Scene();

		SceneObject& GetBackground();
		SceneObject& GetForeground();

		SceneObject* GetActiveCamera();
		void SetActiveCamera(SceneObject * value);

		std::vector<SceneObject*> & GetPointLights();
		std::vector<SceneObject*> & GetSpotLights();

		Core::Property<SceneObject*> AmbientLight;

		SceneObject * GetAmbientLight() const;
		void SetAmbientLight(SceneObject* value);

	private:
		SceneObject* _Background;
		SceneObject* _Foreground;
		SceneObject* _ActiveCamera;

		SceneObject* _AmbientLight;
		std::vector<SceneObject*> _PointLights;
		std::vector<SceneObject*> _SpotLights;
	};
}