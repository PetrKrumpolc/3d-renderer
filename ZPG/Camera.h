#pragma once
#include "IComponent.h"
#include "Size.h"

#include <glm/mat4x4.hpp>
#include <glm/gtc/matrix_transform.hpp>

namespace Entities::Components
{
	class Camera : public Entities::Components::IComponent
	{
	public:
		Camera(const Core::User::Size & resolution, const glm::vec3 & eye, const glm::vec3 & direction, float fovDegree, float near, float far);
		virtual ~Camera() override;

		void SetResolution(const Core::User::Size & value);
		const Core::User::Size& GetResolution() const;

		void SetEye(const glm::vec3 & value);
		glm::vec3 GetEye();

		void SetDirection(const glm::vec3 & value);

		const glm::mat4 & GetProjectionMatrix() const;
		const glm::mat4 & GetViewMatrix() const;


		virtual Entities::Components::ComponentTypes GetType() const noexcept override;

	private:
		Core::User::Size _Resolution;
		float _FieldOfView;
		float _Near;
		float _Far;

		glm::vec3 _Eye;
		glm::vec3 _Direction;

		glm::mat4 _ProjectionMatrix;
		glm::mat4 _ViewMatrix;

		void UpdateProjectionMatrix();
		void UpdateViewMatrix();
	};
}