#pragma once
#include <GL\glew.h>

namespace Rendering::Objects
{
	class VertexLayout
	{
	public:
		VertexLayout();
		~VertexLayout();

		void AddChunk(int index, size_t size, GLenum dataType, bool isNormalized, size_t stride, const void * offset);

		void Bind();
		void Unbind();

	private:
		GLuint _Id;
	};
}