#pragma once
#include <GL\glew.h>

#include <string>

#include "TextureTypes.h"

namespace Rendering::Objects
{
	class Texture
	{
	public:

		Rendering::Objects::TextureTypes GetType() const;


		Texture(const std::string & image, Rendering::Objects::TextureTypes type);
		~Texture();

		void Bind();
		void SetImage(size_t width, size_t height, const void* data, GLenum imageType);

	private:
		GLuint							 _Texture;
		Rendering::Objects::TextureTypes _Type;
	};
}