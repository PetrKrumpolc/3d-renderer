#include "SpotLight.h"

#pragma region Usings
using Entities::Components::SpotLight;
using Entities::Components::PointLight;
using Entities::Components::ComponentTypes;
#pragma endregion

#pragma region Properties
float SpotLight::GetInnerCone() const
{
	return _InnerCone;;
}

void SpotLight::SetInenrCone(float value)
{
	_InnerCone = value;
}

float SpotLight::GetOuterCone() const
{
	return _OuterCone;
}

void SpotLight::SetOuterCone(float value)
{
	_OuterCone = value;
}

ComponentTypes SpotLight::GetType() const noexcept
{
	return ComponentTypes::SpotLight;
}
#pragma endregion


#pragma region Construction & Destruction
SpotLight::SpotLight()
	: PointLight(),
	  _InnerCone(0.91f),
	  _OuterCone(0.82f)
{}

SpotLight::~SpotLight()
{
}
#pragma endregion
