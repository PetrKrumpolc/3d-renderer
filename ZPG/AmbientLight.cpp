#include "AmbientLight.h"

#pragma region Usings
using Entities::Components::IComponent;
using Entities::Components::AmbientLight;
using Entities::Components::ComponentTypes;

using glm::vec3;
#pragma endregion

#pragma region Construction & Destruction
AmbientLight::AmbientLight()
	: _AmbientColor(vec3(0.05f, 0.05f, 0.05f))
{
}

AmbientLight::~AmbientLight()
{
}
#pragma endregion

ComponentTypes AmbientLight::GetType() const noexcept
{
	return ComponentTypes::AmbientLight;
}


const vec3& AmbientLight::GetAmbientColor() const
{
	return _AmbientColor;
}

void AmbientLight::SetAmbientColor(const vec3& value)
{
	_AmbientColor = value;
}
