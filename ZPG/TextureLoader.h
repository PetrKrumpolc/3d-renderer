#pragma once
#include "Texture.h"

#include <string>


namespace Rendering::Objects
{
	class TextureLoader
	{
	public:
		TextureLoader()  = default;
		~TextureLoader() = default;

		Rendering::Objects::Texture* LoadTexture(const std::string & path, Rendering::Objects::TextureTypes type) const;
	};
}