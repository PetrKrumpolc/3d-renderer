#include "InputContext.h"

#include <algorithm>

#pragma region Usings
using Input::InputContext;
using Input::Action;

using std::function;
#pragma endregion

#pragma region Construction & Destruction
InputContext::InputContext()
{
}

InputContext::~InputContext()
{
	ForEach([](Action* action) { delete action; });
}
#pragma endregion


void InputContext::ForEach(std::function<void(Action*)> function)
{
	std::for_each(_Actions.begin(), _Actions.end(), [&function](Action* action) { function(action); });
}

void InputContext::Add(Action* action)
{
	_Actions.push_front(action);
}
