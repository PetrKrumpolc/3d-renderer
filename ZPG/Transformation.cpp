#include "Transformation.h"
#include "SceneObject.h"

#pragma region Usings
using Entities::SceneObject;

using Entities::Components::IComponent;
using Entities::Components::Transformation;
using Entities::Components::ComponentTypes;
using Entities::Components::Rotation;

using Core::Events::Event;

using glm::vec3;
using glm::mat4;
#pragma endregion

#pragma region Construction & Destruction
Transformation::Transformation()
	: _Position(vec3(0.0f)),
	 _Scale(vec3(1.0f)),
	 _Matrix(mat4(1.0f)),
	 PositionChanged(Event<const vec3 &>(&_OnPositionChanged)),
	 TransformationChanged(Event<const mat4 &>(&_OnTransformationChanged))
{
	_Rotation.DirectionChanged.Subscribe(this, &Transformation::OnDirectionChanged);
	UpdateMatrix();
}

Transformation::~Transformation()
{}
#pragma endregion


ComponentTypes Transformation::GetType() const noexcept
{
	return ComponentTypes::Transformation;
}

#pragma region Transformation
const vec3 & Transformation::GetPosition() const noexcept
{
	return _Position;
}

void Transformation::SetPosition(const vec3 & value) noexcept
{
	_Position = value;
	_OnPositionChanged(value);
	UpdateMatrix();
}

Rotation & Transformation::GetRotation() noexcept
{
	return _Rotation;
}

const vec3 & Transformation::GetScale() const noexcept
{
	return _Scale;
}

void Transformation::SetScale(const vec3 & value) noexcept
{
	_Scale = value;
	UpdateMatrix();
}
#pragma endregion

const mat4 & Transformation::GetMatrix() noexcept
{
	return _Matrix;
}


void Transformation::UpdateMatrix()
{	
	_Matrix = glm::translate(_ParentMatrix, _Position);
	_Matrix *= _Rotation.GetRotationMatrix();
	_Matrix = glm::scale(_Matrix, _Scale);

	_OnTransformationChanged(_Matrix);
}


void Transformation::OnDirectionChanged(const vec3& direction)
{
	UpdateMatrix();
}

void Transformation::OnParentTransformChanged(const mat4& matrix)
{
	_ParentMatrix = matrix;
	UpdateMatrix();
}

void Transformation::OnParentChanged(SceneObject* oldParent, SceneObject* newParent)
{
	if (oldParent != nullptr && oldParent->HasComponent(ComponentTypes::Transformation))
		oldParent->GetComponent<Transformation>(ComponentTypes::Transformation)->TransformationChanged.Unsubscribe(this, &Transformation::OnParentTransformChanged);

	if (newParent != nullptr && newParent->HasComponent(ComponentTypes::Transformation))
	{
		Transformation* transform = newParent->GetComponent<Transformation>(ComponentTypes::Transformation);
		transform->TransformationChanged.Subscribe(this, &Transformation::OnParentTransformChanged);

		_ParentMatrix = transform->GetMatrix();
	}
	else
		_ParentMatrix = mat4(1.0f);

	UpdateMatrix();
}


void Transformation::OnOwnerChanged(SceneObject* oldOwner, SceneObject* newOwner)
{
	if (oldOwner != nullptr)
		oldOwner->ParentChanged.Unsubscribe(this, &Transformation::OnParentChanged);

	if (newOwner != nullptr)
		newOwner->ParentChanged.Subscribe(this, &Transformation::OnParentChanged);
}
