#pragma once
#include "ShaderPipeline.h"
#include "ShaderPipelineTypes.h"
#include "ShaderPipelineFactory.h"

#include <forward_list>


namespace Rendering::Shaders
{
	class ShaderPipelineRepository
	{
	public:
		ShaderPipelineRepository()  = default;
		~ShaderPipelineRepository();

		void Preload();

		Rendering::Shaders::ShaderPipeline & GetItem(Rendering::Shaders::ShaderPipelineTypes type);

	private:
		Rendering::Shaders::ShaderPipeline * FindPipeline(Rendering::Shaders::ShaderPipelineTypes type);
		Rendering::Shaders::ShaderPipeline&	 LetCreatePipeline(Rendering::Shaders::ShaderPipelineTypes type);

		Rendering::Shaders::ShaderPipelineFactory			   _PipelineFactory;
		std::forward_list<Rendering::Shaders::ShaderPipeline*> _Pipelines;
	};
}