#include "LoggerFactory.h"
#include "ConsoleLogger.h"
#include "FileLoggerDecorator.h"
#include "DateTime.h"

#pragma region Usings
using Core::Debug::ILogger;
using Core::Debug::LoggerFactory;
using Core::Debug::ConsoleLogger;
using Core::Debug::FileLoggerDecorator;

using Core::Time::DateTime;

using std::string;
#pragma endregion

ILogger* LoggerFactory::CreateLogger() const
{
#ifdef LOG_FILE
	return new FileLoggerDecorator(GetFileName(), new ConsoleLogger());
#else 
	return new ConsoleLogger();
#endif
}


string LoggerFactory::GetFileName() const
{
	return "logs/log " + DateTime::Now().ToString("%Y %m %d %H_%M_%S") + ".txt";
}