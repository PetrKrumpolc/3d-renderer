#include "KeyboadGesture.h"

#pragma region Usings
using Core::User::Input::Gestures::IGesture;
using Core::User::Input::Gestures::KeyboardGesture;
using Core::User::Input::Gestures::GestureType;
#pragma endregion

#pragma region Properties
int KeyboardGesture::GetButton() const
{
	return _Button;
}

int KeyboardGesture::GetModifiers() const
{
	return _Modiffiers;
}
#pragma endregion

#pragma region Construction & Destruction
KeyboardGesture::KeyboardGesture(int button)
	: IGesture(GestureType::KeyStroke),
	 _Button(button),
	 _Modiffiers(0)
{}

KeyboardGesture::KeyboardGesture(int button, int modifiers)
	: IGesture(GestureType::KeyStroke), 
	 _Button(button),
	 _Modiffiers(modifiers)
{}

KeyboardGesture::~KeyboardGesture()
{}
#pragma endregion
