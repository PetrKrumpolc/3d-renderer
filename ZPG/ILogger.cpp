#include "ILogger.h"
#include "DateTime.h"

#include <memory>
#include <sstream>
#include <iomanip>
#include <ctime>

#pragma region Usings
using Core::Debug::ILogger;
using Core::Time::DateTime;

using std::string;
using std::stringstream;
using std::unique_ptr;
#pragma endregion


string ILogger::FormatLogMessage(const string & type, const string & message, const string& timeFormat)
{
	stringstream output;

	output << DateTime::Now().ToString(timeFormat);
	output << " [" << type << "] ";
	output << "- ";
	output << message;

	return output.str();
}
