#include "SceneObject.h"

#include <algorithm>

#pragma region Usings
using Entities::SceneObject;
using Entities::Components::IComponent;
using Entities::Components::ComponentTypes;

using Core::Events::Event;

using std::string;
using std::function;
#pragma endregion

#pragma region Properties
const string& SceneObject::GetName() const
{
	return _Name;
}

void SceneObject::SetName(const string & value)
{
	_Name = value;
}

unsigned char SceneObject::GetId() const
{
	return _Id;
}

void SceneObject::SetId(unsigned char value)
{
	_Id = value;
}

SceneObject* SceneObject::GetParent()
{
	return _Parent;
}
#pragma endregion

#pragma region Construction & Destruction
SceneObject::SceneObject()
	:_Name("Scene Object"),
	 _Parent(nullptr),
	 _Id(0),
	 ParentChanged(Event<SceneObject*, SceneObject*>(&_OnParentChanged))
{}

SceneObject::~SceneObject()
{
	std::for_each(_Children.begin(),   _Children.end(),   [](SceneObject* child)     { delete child; });
	std::for_each(_Components.begin(), _Components.end(), [](IComponent*  component) { delete component; });
}
#pragma endregion



void SceneObject::Attach(SceneObject* child)
{
	SceneObject* oldParent = child->_Parent;

	_Children.push_front(child);
	child->_Parent = this;

	child->_OnParentChanged(oldParent, this);
}

void SceneObject::Attach(IComponent* component)
{
	_Components.push_front(component);
	component->SetOwner(this);
}

bool SceneObject::HasComponent(ComponentTypes type) const noexcept
{
	for (IComponent* component : _Components)
		if (component->GetType() == type)
			return true;

	return false;
}



void SceneObject::ForEach(function<void(SceneObject*)> action)
{
	action(this);

	for (SceneObject* child : _Children)
		child->ForEach(action);
}



const string& SceneObject::ToString()
{
	return _Name;
}
