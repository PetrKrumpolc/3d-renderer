#pragma once
#include <GL/glew.h>
#include <GLFW\glfw3.h>

#include "IInputDevice.h"
#include "Point.h"

namespace Core::User::Input
{
	class Mouse : public IInputDevice
	{
	public:
		Mouse(GLFWwindow& owner);
		virtual ~Mouse() override;

		Core::User::Input::InputType GetType() const override;

		virtual void Flush() override;

		virtual bool IsActive(const Core::User::Input::Gestures::IGesture& gesture) const override;

		const Core::User::Point & GetPosition() const;
		const Core::User::Point GetPositionDelta() const;
		const Core::User::Point & GetScrollOffset() const;

		bool IsPressed(int buttonId) const;
		bool IsScrolled() const;
		bool IsMoved()    const;

	private:
		GLFWwindow& _Owner;

		Core::User::Point _Position;
		Core::User::Point _PrevPosition;
		Core::User::Point _ScrollOffset;
		Core::User::Point _PrevScrollOffset;

		bool _IsPressed[8];

		void OnMove(double x, double y);
		void OnClick(int button, int action, int mode);
		void OnScroll(double xOffset, double yOffset);
	};
}