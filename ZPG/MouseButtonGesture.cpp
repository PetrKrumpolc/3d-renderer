#include "MouseButtonGesture.h"

#pragma region Usings
using Core::User::Input::Gestures::IGesture;
using Core::User::Input::Gestures::MouseButtonGesture;
using Core::User::Input::Gestures::GestureType;
#pragma endregion

#pragma region Properties
int MouseButtonGesture::GetButton() const
{
	return _Button;
}
#pragma endregion

#pragma region Construction & Destruction
MouseButtonGesture::MouseButtonGesture(int button)
	: IGesture(GestureType::MouseButton),
	 _Button(button)
{}

MouseButtonGesture::~MouseButtonGesture()
{}
#pragma endregion
