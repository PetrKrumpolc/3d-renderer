#pragma once
#include <GL\glew.h>

namespace Rendering::Objects
{
	enum class TextureTypes
	{
		Diffuse   = GL_TEXTURE0,
		Specular  = GL_TEXTURE1,
		Normal    = GL_TEXTURE2,
		Occlusion = GL_TEXTURE3
	};
}