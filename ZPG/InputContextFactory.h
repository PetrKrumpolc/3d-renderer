#pragma once
#include "InputContext.h"
#include "Scene.h"

namespace Input
{
	class InputContextFactory
	{
	public:
		InputContextFactory(Entities::Scene* scene);
		~InputContextFactory() = default;

		Input::InputContext* CreateContext();
	private:
		Entities::Scene * _Scene;
	};
}