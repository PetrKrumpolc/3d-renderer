#include "Action.h"

#pragma region Usings
using Input::Action;
using Input::ActionType;

using Input::Commands::ICommand;

using Core::User::Input::Gestures::IGesture;
#pragma endregion

#pragma region Properties
void Action::SetGesture(IGesture* value)
{
	_Gesture = value;
}

IGesture* Input::Action::GetGesture()
{
	return _Gesture;
}

void Action::SetCommand(ICommand* command)
{
	if (_Command != nullptr)
		delete _Command;

	_Command = command;
}
#pragma endregion


#pragma region Construction & Destruction
Action::Action(ActionType type)
	:_Type(type)
{}

Action::Action(ActionType type, IGesture * gesture)
	: _Type(type),
	  _Gesture(gesture)
{
}

Action::Action(ActionType type, IGesture * gesture, ICommand* command)
	: _Type(type),
	  _Gesture(gesture),
	  _Command(command)
{}

Action::~Action()
{
	if (_Gesture != nullptr)
		delete _Gesture;
}
#pragma endregion


void Action::Invoke(float timeDelta) const
{
	if (_Command != nullptr)
		_Command->Execute(timeDelta);
}
