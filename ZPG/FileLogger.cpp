#include "FileLoggerDecorator.h"
#include "TextWriter.h"

#include <fstream>
#include <sstream>

#pragma region Usings
using Core::Debug::FileLogger;
using Core::IO::TextWriter;

using std::string;
using std::stringstream;
using std::unique_ptr;
using std::ofstream;
using std::ios;
using std::endl;

using std::for_each;
#pragma endregion


#pragma region Construction & Destruction
FileLogger::FileLogger(string path, unique_ptr<ILogger> logger)
	:_CacheLimit(25),
	 _CacheSize(0),
	 _Path(path),
	 _Logger(std::move(logger))
{}

FileLogger::~FileLogger()
{
	if(_CacheSize > 0)
		SaveToFile();
}
#pragma endregion

#pragma region ILogger
void FileLogger::LogError(string message)
{
	_Logger->LogError(message);
	AfterLogging(FormatLogMessage("Error", message, "%Y %m %d %X"));
}

void FileLogger::LogInfo(string message)
{
	_Logger->LogInfo(message);
	AfterLogging(FormatLogMessage("Info", message, "%Y %m %d %X"));
}

void FileLogger::LogWarning(string message)
{
	_Logger->LogWarning(message);
	AfterLogging(FormatLogMessage("Warning", message, "%Y %m %d %X"));
}
#pragma endregion

#pragma region Cache
void FileLogger::ResetCache()
{
	_CacheSize = 0;
	_Cache.clear();
}

void FileLogger::AppendToCache(const string& message)
{
	_Cache << message << endl;
	_CacheSize++;
}
#pragma endregion

void FileLogger::AfterLogging(const string& message)
{
	AppendToCache(message);

	if (_CacheSize >= _CacheLimit)
	{
		SaveToFile();
		ResetCache();
	}
}

void FileLogger::SaveToFile()
{
	TextWriter(new ofstream(_Path, ios::out | ios::ate)).Write(_Cache);
}
