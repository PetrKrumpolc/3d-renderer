#pragma once
#include "IComponent.h"
#include <glm/gtc/type_ptr.hpp>

namespace Entities::Components
{
	class PointLight : public Entities::Components::IComponent
	{
	public:
		PointLight();
		virtual ~PointLight() override;

		virtual Entities::Components::ComponentTypes GetType() const noexcept override;

		float GetPower() const;
		void SetPower(float value);

		const glm::vec3& GetDiffuseColor() const;
		void SetDiffuseColor(const glm::vec3& value);

		const glm::vec3& GetSpecularColor() const;
		void SetSpecularColor(const glm::vec3& value);

		float GetShininess() const;
		void SetShininess(float value);

		float GetConstant() const;
		void SetConstant(float value);

		float GetLinear() const;
		void SetLinear(float value);

		float GetQuadratic() const;
		void SetQuadratic(float value);

	private:
		glm::vec3 _DiffuseColor;
		glm::vec3 _SpecularColor;

		float _Shininess;
		float _Constant;
		float _Linear;
		float _Quadratic;

		float _Power;
	};
}