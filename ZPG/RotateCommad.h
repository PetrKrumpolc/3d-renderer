#pragma once
#include "ICommand.h"
#include "Mouse.h"
#include "Scene.h"

namespace Input::Commands
{
	class RotateComamnd : public Input::Commands::ICommand
	{
	public:
		RotateComamnd(Core::User::Input::Mouse & mouse, Entities::Scene & scene, float sensitivity);
		virtual ~RotateComamnd() override;

		virtual void Execute(float timeDelta) override;

	private:
		Core::User::Input::Mouse& _Mouse;
		Entities::Scene&          _Scene;

		const float _Sensitivity;
	};
}