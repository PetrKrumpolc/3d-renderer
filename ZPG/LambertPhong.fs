#version 420

#define USE_DIFFUSE_MAP  1
#define USE_SPECULAR_MAP 4
#define USE_NORMAL_MAP   2

struct Material
{
	vec3 Diffuse;
	vec3 Specular;
	vec3 Ambient;

	float Shininnes;
};

struct PointLight
{
    vec3 Position;

    vec3 Diffuse;
    vec3 Specular;

	float Constant;
	float Linear;
	float Quadratic;

	float Power;
};

struct SpotLight
{
	PointLight Base;

	vec3 Direction;

	float InnerCone;
	float OuterCone;
};

struct ResultColor
{
    vec3 Diffuse;
    vec3 Specular;
};

in vec3 ex_worldPosition;
in vec3 ex_Normal;
in vec3 ex_Tangent;
in vec2 ex_uv;

out vec4 frag_colour;

uniform vec3        eyePosition;

uniform PointLight  pointLights[16];
uniform SpotLight   spotLights[16];
uniform vec3        ambientLight;

uniform int         pointLightCount;
uniform int         spotLightCount;

uniform Material     material;
uniform unsigned int materialSettings;

uniform sampler2D diffuseMap;
uniform sampler2D specularMap;
uniform sampler2D normalMap;

vec3 calcNormal() 
{
    if((materialSettings & USE_NORMAL_MAP) == 0)
        return normalize(ex_Normal);

    vec3 normal  = normalize(ex_Normal);
    vec3 tangent = normalize(ex_Tangent);

        //Gram�Schmidt process
    tangent = normalize(tangent - dot(tangent, normal) * normal);       
    vec3 bitangent = cross(tangent, normal);
    vec3 bumpMapNormal = texture(normalMap, ex_uv).xyz;

        //p�evod z vektoru barvy o rozsahu <0,1> do vektoru norm�ly <-1,1>
    bumpMapNormal = 2.0 * bumpMapNormal - vec3(1.0,1.0,1.0); 

        // Transforma�n�  matice TBN
    mat3 TBN = mat3(tangent, bitangent, normal);   
    return normalize(TBN * bumpMapNormal);
    //return normalize(bumpMapNormal);
}

ResultColor ComputePointLight(PointLight light)
{
    vec3  normal           = calcNormal();
    vec3  diffuseDir       = light.Position - ex_worldPosition;
    vec3  reflectionDir    = reflect(normalize(-diffuseDir), normal);
    vec3  viewDir          = eyePosition - ex_worldPosition;

    float diffuseIntensity  = max(dot(normal, normalize(diffuseDir)), 0.0);
    float specularIntensity = pow(max(dot(normalize(viewDir), normalize(reflectionDir)), 0.0), material.Shininnes);

	float distance    = length(diffuseDir);
	float attenuation = 1.0 / (light.Constant + light.Linear * distance + light.Quadratic * (distance * distance));

    ResultColor color;

    color.Diffuse  = light.Diffuse  * material.Diffuse  * diffuseIntensity  * attenuation * light.Power;
    color.Specular = light.Specular * material.Specular * specularIntensity * attenuation * light.Power;

    return color;
}

ResultColor ComputeSpotLight(SpotLight light)
{
	vec3  lightDir  = light.Base.Position - ex_worldPosition;
	float angle     = dot(normalize(lightDir), normalize(light.Direction));

	ResultColor result;

	if(angle > light.OuterCone)
	{
		float edgeAngle = light.InnerCone - light.OuterCone;
		float intensity = clamp((angle - light.OuterCone) / edgeAngle, 0.0, 1.0);

		result = ComputePointLight(light.Base);

		result.Diffuse  *= intensity;
		result.Specular *= intensity;
	}
	else
	{
		result.Diffuse  = vec3(0.0);
		result.Specular = vec3(0.0);
	}

	return result;
}


void main() 
{
    vec3 diffuseTotal  = vec3(0.0);
    vec3 ambient       = material.Ambient * ambientLight;
    vec3 specularTotal = vec3(0.0);


    for(int i = 0; i < pointLightCount; i++)
    {
        ResultColor c = ComputePointLight(pointLights[i]);

        diffuseTotal  += c.Diffuse;
        specularTotal += c.Specular;
    }

	for(int i = 0; i < spotLightCount; i++)
    {
        ResultColor c = ComputeSpotLight(spotLights[i]);

        diffuseTotal  += c.Diffuse;
        specularTotal += c.Specular;
    }

    if((materialSettings & USE_SPECULAR_MAP) != 0)
        specularTotal *= texture2D(specularMap, ex_uv).xyz;

    if((materialSettings & USE_DIFFUSE_MAP) != 0)
        diffuseTotal *= texture2D(diffuseMap, ex_uv).xyz;

    frag_colour = vec4(ambient + diffuseTotal + specularTotal, 1.0f);
}