#include "PrimitivesFactory.h"
#include "Mesh.h"
#include "Transformation.h"

static float TRIANGLE[] = {
	0.0f, 0.5f, 0.0f,
	0.5f, -0.5f, 0.0f,
	-0.5f, -0.5f, 0.0f
};

#pragma region Usings
using Rendering::Objects::PrimitivesFactory;
using Rendering::Objects::Mesh;

using Entities::SceneObject;
using Entities::Components::Transformation;
#pragma endregion



SceneObject* PrimitivesFactory::CreateTriangle() const
{
	SceneObject* triangle = new SceneObject();

	triangle->SetName("Primitive - Triagle");
	triangle->Attach(new Transformation());
	triangle->Attach(new Mesh(TRIANGLE, sizeof(TRIANGLE), 3));

	return triangle;
}
