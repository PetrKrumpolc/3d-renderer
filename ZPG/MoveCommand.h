#pragma once
#include "ICommand.h"
#include "MoveType.h"
#include "Scene.h"
#include "Transformation.h"

#include <unordered_map>
#include <functional>

namespace Input::Commands
{
	class MoveCommand : public Input::Commands::ICommand
	{
	public:
		MoveCommand(Entities::Scene & scene, float step, Input::Commands::MoveType moveType);
		virtual ~MoveCommand() override;

		virtual void Execute(float timeDelta) override;

	private:
		Entities::Scene& _Scene;

		const float						_Step;
		const Input::Commands::MoveType _MoveType;

		static std::unordered_map<Input::Commands::MoveType, std::function<void(float step, Entities::Components::Transformation& transform)>> _Movements;
	};
}