#pragma once

namespace Core
{
	template<typename T>
	class ReadOnlyProperty
	{
	public:
		ReadOnlyProperty(const ReadOnlyProperty<T>&) = delete;
		ReadOnlyProperty(T && value)
			: _Value(value)
		{}
		~ReadOnlyProperty() = default;

		ReadOnlyProperty<T>& operator = (ReadOnlyProperty<T>&) = delete;

		inline operator T& () { return _Value; }
		inline T& operator () () { return _Value; }


	private:
		T && _Value;
	};
}