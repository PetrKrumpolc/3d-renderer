#include "Material.h"

#pragma region Usings
using Rendering::Objects::Material;
using Rendering::Objects::MaterialConfig;
using Rendering::Objects::Texture;
using Rendering::Objects::MaterialSettings;

using std::string;
#pragma endregion


#pragma region Properties
Texture* Material::GetDiffuseMap()
{
	return _DiffuseMap;
}

void Material::SetDiffuseMap(Texture* value)
{
	if (_DiffuseMap != nullptr)
		delete _DiffuseMap;

	_DiffuseMap = value;
	SetOption(MaterialSettings::UseDiffuseMap, value != nullptr);
}

Texture* Material::GetNormalMap()
{
	return _NormalMap;
}

void Material::SetNormalMap(Texture* value)
{
	if (_NormalMap != nullptr)
		delete _NormalMap;

	_NormalMap = value;
	SetOption(MaterialSettings::UseNormalMap, value != nullptr);
}

Texture* Material::GetSpecularMap()
{
	return _SpecularMap;
}

void Material::SetSpecularMap(Texture* value)
{
	if (_SpecularMap != nullptr)
		delete _SpecularMap;

	_SpecularMap = value;
	SetOption(MaterialSettings::UseSpecualMap, value != nullptr);
}

Texture* Material::GetOcclusionMap()
{
	return _OcclusionMap;
}

void Material::SetOcclusionMap(Texture* value)
{
	if (_OcclusionMap != nullptr)
		delete _OcclusionMap;

	_OcclusionMap = value;
}

MaterialConfig& Material::GetConfiguration()
{
	return _Config;
}


void Material::SetOption(MaterialSettings option, bool value)
{
	if (value)
		_Settings = static_cast<MaterialSettings>(static_cast<unsigned int>(_Settings) | static_cast<unsigned int>(option));
	else
		_Settings = static_cast<MaterialSettings>(static_cast<unsigned int>(_Settings) & ~static_cast<unsigned int>(option));
}


const string& Material::GetName() const
{
	return _Name;
}
void Material::SetName(const string& value)
{
	_Name = value;
}

MaterialSettings& Material::GetSettings()
{
	return _Settings;
}
#pragma endregion



#pragma region Construction & Destruction
Material::Material()
	: _DiffuseMap(nullptr),
	  _NormalMap(nullptr),
	  _SpecularMap(nullptr),
	  _OcclusionMap(nullptr)
{
}

Material::~Material()
{
	SetDiffuseMap(nullptr);
	SetNormalMap(nullptr);
	SetSpecularMap(nullptr);
	SetOcclusionMap(nullptr);
}
#pragma endregion